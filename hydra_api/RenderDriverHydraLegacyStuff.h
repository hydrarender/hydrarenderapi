#pragma once

#include <windows.h>
#include <cstdint>
#include <fstream>
#include <vector>

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct ZBlockT2
{
  ZBlockT2() { index = 0; diff = 100; counter = 0; index2 = 0; }

  ZBlockT2(int a_index, float a_diff)
  {
    index = a_index;
    index2 = 0;
    diff = a_diff;
    counter = 0;
  }

  int index;   // just block offset if global screen buffer
  int index2;  // index in other buffer + avg trace depth
  int counter; // how many times this block was traced?
  float diff;  // error in some units. stop criterion if fact
};

enum ZIMAGE_FLAGS { ZIMAGE_IS_EMPTY = 1 };

struct ImageZInfo
{
  int   width;
  int   height;
  short flags;
  short layersNum;
  float mltAvgImageBrightness;
};

struct ImageZ
{
  ImageZ(const char* imageFileName, const char* mutexName, int width, int height, std::ostream* plog, int layersNum = 1, bool a_attach = false);
  virtual ~ImageZ();

  void Clear();
  bool Lock(int a_miliseconds);
  void Unlock();
  bool IsEmpty() const;

  void  UnionWith(const ImageZ* a_pOtherImage);
  float EstimateProgress(float relError, int minRaysPerPixel, int maxRaysPerPixel);

  float*  ColorPtr(int color_id = 0) { return colors[color_id]; }
  const float* ColorPtr(int color_id = 0)  const  { return colors[color_id]; }

  ZBlockT2* ZBlocksPtr() { return zblocks; }
  char*     message()     { return commands; }
  const char* message() const { return commands; }

  void SetNotEmpty() { m_pInfo->flags = 0; }
  int ZBlocksNum() { return (m_pInfo->width*m_pInfo->height) / ZBLOCKSIZE; }

  enum { ZBLOCKSIZE = 256, AUX_COLORS_MAX = 16 };

  bool IsValid() const { return (colors[0] != nullptr); }
  void SaveToFile(const wchar_t* fname);
  bool LoadFromFile(const wchar_t* fname);

  void SaveToBMP(const wchar_t* fname, float a_gamma);
  void SaveToImage4f(const wchar_t* a_path);

  int Width()  const { if (m_pInfo == nullptr) return 0; else return m_pInfo->width; }
  int Height() const { if (m_pInfo == nullptr) return 0; else return m_pInfo->height; }

  bool SendMsg(const char* a_msg, bool inc_mid = true);
  const char* RecieveMsg();

  bool wasFinalOnce;

protected:

  void InitFromBufferData();

  float*    colors[AUX_COLORS_MAX];
  ZBlockT2* zblocks;
  char*     commands;

  HANDLE m_mutex;
  HANDLE m_imageFile;

  ImageZInfo* m_pInfo;
  int m_zblockCount;

  ImageZ(const ImageZ& a_rhs) {}
  ImageZ& operator=(const ImageZ& a_rhs) { return *this; }

  uint64_t m_bufferByteSize;
  void*    m_bufferData;

  int  lastMsgId;
  int  lastMsgRcv;
  char lastMsg[1024];

  std::ostream* m_pLog;

};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


enum SENDED_IMAGE_TYPE {
  SEND_IMAGE_LDR = 0,
  SEND_IMAGE_HDR_FLOAT4 = 1,
  SEND_IMAGE_HDR_UINT16 = 2,
};

struct ImageZ; // forward decl

struct HydaRenderDevice
{
  std::wstring name;
  std::wstring driverName;
  int          id;
  bool         isCPU;
};


struct RenderProcessRunParams
{
  RenderProcessRunParams() : debug(false), normalPriorityCPU(false), showConsole(true), compileShaders(false), enableMLT(false) {}

  bool debug;
  bool normalPriorityCPU;
  bool showConsole;
  bool compileShaders;
  bool enableMLT;

  std::string customExePath;
  std::string customExeArgs;
  std::string customLogFold;
};

struct IHydraNetPluginAPI
{
  IHydraNetPluginAPI(){}
  virtual ~IHydraNetPluginAPI(){}

  virtual void updatePresets(const char* a_presetsXML) = 0;
  virtual bool hasConnection() const = 0;
  virtual bool isStatic() { return false; }

  virtual void runAllRenderProcesses(RenderProcessRunParams a_params, const std::vector<HydaRenderDevice>& a_devList) = 0;
  virtual void stopAllRenderProcesses() = 0;

  virtual ImageZ* imageA() { return nullptr; }
  virtual ImageZ* imageB() { return nullptr; }

  virtual HANDLE  getMtlRenderHProcess() const { return 0; }
};


enum PROGRAM_IDS{
  HYDRA_SERVER_ID = 1,
  HYDRA_PLUGIN_ID = 2,
  HYDRA_SERVER_FINISH_ID = 3,
};

struct SharedBufferDataInfo
{
  int width;
  int height;
  int read;
  int written;
};


bool isTargetDevIdACPU(int a_devId, const std::vector<HydaRenderDevice>& a_devList);
bool isTargetDevIdAHydraCPU(int a_devId, const std::vector<HydaRenderDevice>& a_devList);

IHydraNetPluginAPI* CreateHydraServerConnection(int renderWidth, int renderHeight, bool inMatEditor, const std::vector<int>& a_devList);
//std::wstring HydraInstallPathW();
std::string  ws2s(const std::wstring& s);
std::wstring s2ws(const std::string& s);
std::vector<HydaRenderDevice> InitDeviceList(int mode);



namespace ZImageUtils
{
  struct float2;
  struct float3;
  struct float4;

  static inline float MonteCarloRelErr(float3 avgColor, float sqrColor, int nSamples);
  static inline uint32_t ZIndexHost(uint16_t x, uint16_t y);
  void initTables();
  inline uint32_t IndexZB(int x, int y, int pitch);
  float BlockError(int offset, float* a_color, int nSamples, float a_pathTraceError);
  static bool BlockFinished(ZBlockT2 block, int a_minRaysPerPixel, int a_maxRaysPerPixel);
  void ImageZBlockMemToRowPitch(const float* inData, float* outData, int w, int h);
  inline uint32_t IndexZB(int x, int y, int pitch);
  void initTables();
};
