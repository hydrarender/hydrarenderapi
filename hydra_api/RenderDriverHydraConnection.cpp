#include "HydraRenderDriverAPI.h"

#include <vector>
#include <string>
#include <sstream>
#include <fstream>

//#include "HydraInternal.h" // #TODO: this is only for hr_mkdir and hr_cleardir. Remove this further
#include "RenderDriverHydraLegacyStuff.h"

#include "HydraXMLHelpers.h"

#include "HR_HDRImage.h"

#include <mutex>
#include <future>
#include <thread>

#pragma warning(disable:4996) // for wcscpy to be ok

static constexpr bool FB_UPDATE_MT        = false; // #TODO: figure out things around std::slepp_for, thread and async - they are not so simple.
static constexpr bool MODERN_DRIVER_DEBUG = false;

using HydraRender::HDRImage4f;

struct RD_HydraConnection : public IHRRenderDriver
{
  RD_HydraConnection() : m_pConnection(nullptr),      m_progressVal(0.0f),     m_firstUpdate(true), m_width(0), m_height(0), m_avgBrightness(0.0f), m_avgBCounter(0),
                         m_enableMedianFilter(false), m_medianthreshold(0.4f), m_stopThreadImmediately(false), haveUpdateFromMT(false), m_threadIsRun(false), m_threadFinished(false), hadFinalUpdate(false),
                         m_pColorBuffer(nullptr)
  {
    m_msg = L"";
    InitBothDeviceList();

    wcscpy(m_lastServerReply, L"");

    //#TODO: init m_currAllocInfo
    //#TODO: init m_presets
    //#TODO: init m_lastServerReply
  }

  ~RD_HydraConnection()
  {
    ClearAll();
  }

  void              ClearAll();
  HRDriverAllocInfo AllocAll(HRDriverAllocInfo a_info);

  void GetLastErrorW(wchar_t a_msg[256]) override;

  bool UpdateImage(int32_t a_texId, int32_t w, int32_t h, int32_t bpp, const void* a_data, pugi::xml_node a_texNode) override;
  bool UpdateMaterial(int32_t a_matId, pugi::xml_node a_materialNode) override;

  bool UpdateLight(int32_t a_lightIdId, pugi::xml_node a_lightNode) override;
  bool UpdateMesh(int32_t a_meshId, pugi::xml_node a_meshNode, const HRMeshDriverInput& a_input, const HRBatchInfo* a_batchList, int32_t listSize) override;

  bool UpdateImageFromFile(int32_t a_texId, const wchar_t* a_fileName, pugi::xml_node a_texNode) override;
  bool UpdateMeshFromFile(int32_t a_meshId, pugi::xml_node a_meshNode, const wchar_t* a_fileName) override;

  bool UpdateCamera(pugi::xml_node a_camNode) override;
  bool UpdateSettings(pugi::xml_node a_settingsNode) override;

  /////////////////////////////////////////////////////////////////////////////////////////////

  void BeginScene();
  void EndScene();
  void InstanceMeshes(int32_t a_mesh_id, const float* a_matrices, int32_t a_instNum, const int* a_lightInstId) override;
  void InstanceLights(int32_t a_light_id, const float* a_matrix, pugi::xml_node* a_custAttrArray, int32_t a_instNum, int32_t a_lightGroupId) override;

  void Draw();

  HRRenderUpdateInfo HaveUpdateNow();

  void GetFrameBufferHDR(int32_t w, int32_t h, float*   a_out, const wchar_t* a_layerName) override;
  void GetFrameBufferLDR(int32_t w, int32_t h, int32_t* a_out) override;

  void GetGBufferLine(int32_t a_lineNumber, HRGBufferPixel* a_lineData, int32_t a_startX, int32_t a_endX) override;

  // info and devices
  //
  HRDriverInfo Info();
  const HRRenderDeviceInfoListElem* DeviceList() const;
  void EnableDevice(int32_t id, bool a_enable);

  void ExecuteCommand(const wchar_t* a_cmd, wchar_t* a_out);
  void EndFlush() override;

  void SetLogDir(const wchar_t* a_logDir, bool a_hideCmd) override;

  std::shared_ptr<HydraRender::HDRImage4f> GetFrameBufferImage(const wchar_t* a_imageName) { return m_pColorBuffer; }

protected:

  std::wstring m_libPath;
  std::wstring m_msg;
  std::wstring m_logFolder;
  std::string  m_logFolderS;
  int          m_instancesNum; //// current instance num in the scene; (m_instancesNum == 0) => empty scene! 

  void InitBothDeviceList();
  void GetFrameBufferColorFromABImages(int32_t w, int32_t h, float* a_out);
  void GetFrameBufferColorFromABImagesMLT(int32_t w, int32_t h, float* a_out);
  void UnionABImagesFunc();
  void RunAllHydraHeads();

  void CopyFromZMLT(int32_t w, int32_t h, float* a_out);

  HRDriverAllocInfo m_currAllocInfo;

  // legacy crap
  //
  IHydraNetPluginAPI* m_pConnection;

  std::vector<HydaRenderDevice>           m_devList;
  std::vector<HRRenderDeviceInfoListElem> m_devList2;

  std::shared_ptr<HDRImage4f> m_pColorBuffer; // instead of  std::vector<float> m_fbDataHDR; THIS IS THE MAIN FRAMEBUFFER COLOR !!!
  std::vector<float>          m_gbuffer;

  float m_progressVal;
  RenderProcessRunParams m_params;

  bool m_enableMLT = false;
  bool m_hideCmd   = false;
  bool m_drawTiles = false;

  struct RenderPresets
  {
    int   minrays;
    int   maxrays;
    float errHDR;
  } m_presets;

  bool m_firstUpdate;

  int m_width;
  int m_height;

  float m_avgBrightness;
  int   m_avgBCounter;

  HDRImage4f m_tempImage;

  bool  m_enableMedianFilter;
  float m_medianthreshold;

  bool m_stopThreadImmediately;
  bool m_threadIsRun;
  bool m_threadFinished;
  std::mutex m_abMutex;
  bool haveUpdateFromMT;
  bool hadFinalUpdate;
  wchar_t m_lastServerReply[256];

  std::thread m_updateFbThread;
};


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void RD_HydraConnection::InitBothDeviceList()
{
  m_devList = InitDeviceList(1);
  m_devList2.resize(m_devList.size());

  // form list in memory ... )
  //
  for (size_t i = 0; i < m_devList2.size(); i++)
  {
    const HydaRenderDevice& devInfo = m_devList[i];
    HRRenderDeviceInfoListElem* pListElem = &m_devList2[i];

    pListElem->id = (int32_t)(i);
    pListElem->isCPU = devInfo.isCPU;
    pListElem->isEnabled = false;

    wcscpy(pListElem->name, devInfo.name.c_str());
    wcscpy(pListElem->driver, devInfo.driverName.c_str());

    if (i != m_devList2.size() - 1)
      pListElem->next = &m_devList2[i + 1];
    else
      pListElem->next = nullptr;
  }
}

void RD_HydraConnection::ClearAll()
{
  if (FB_UPDATE_MT)
  {
    while (!m_threadFinished)
    {
      m_stopThreadImmediately = true;
      std::this_thread::sleep_for(std::chrono::microseconds(10));
    }
  }

  delete m_pConnection;
  m_pConnection = nullptr;

  m_presets.errHDR  = 0.01f;
  m_presets.minrays = 256;
  m_presets.maxrays = 2048;
}

HRDriverAllocInfo RD_HydraConnection::AllocAll(HRDriverAllocInfo a_info)
{
  m_libPath       = std::wstring(a_info.libraryPath);
  m_currAllocInfo = a_info;
  return m_currAllocInfo;
}

const HRRenderDeviceInfoListElem* RD_HydraConnection::DeviceList() const
{
  if (m_devList2.size() == 0)
    return nullptr;
  else
    return &m_devList2[0];
}

void RD_HydraConnection::EnableDevice(int32_t id, bool a_enable)
{
  if (id < m_devList2.size())
    m_devList2[id].isEnabled = a_enable;
}

HRDriverInfo RD_HydraConnection::Info()
{
  HRDriverInfo info;

  info.supportHDRFrameBuffer        = true;
  info.supportHDRTextures           = true;
  info.supportMultiMaterialInstance = false;

  info.supportImageLoadFromInternalFormat = false;
  info.supportMeshLoadFromInternalFormat  = false;

  info.supportImageLoadFromExternalFormat = true;
  info.supportLighting                    = true;
  info.createsLightGeometryItself         = false;

  info.memTotal = int64_t(8) * int64_t(1024 * 1024 * 1024);

  return info;
}

void RD_HydraConnection::GetLastErrorW(wchar_t a_msg[256])
{
  wcscpy(a_msg, m_msg.c_str());
}

void RD_HydraConnection::SetLogDir(const wchar_t* a_logDir, bool a_hideCmd)
{
  m_logFolder  = a_logDir;
  m_logFolderS = ws2s(m_logFolder);

  const std::wstring check = s2ws(m_logFolderS);
  if (m_logFolder != check)
    m_msg = L"[RD_HydraConnection]: warning, bad log dir";

  m_hideCmd = a_hideCmd;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


bool RD_HydraConnection::UpdateImage(int32_t a_texId, int32_t w, int32_t h, int32_t bpp, const void* a_data, pugi::xml_node a_texNode)
{
  return true;
}

bool RD_HydraConnection::UpdateImageFromFile(int32_t a_texId, const wchar_t* a_fileName, pugi::xml_node a_texNode)
{
  return true;
}

bool RD_HydraConnection::UpdateMaterial(int32_t a_matId, pugi::xml_node a_materialNode)
{
  return true;
}

bool RD_HydraConnection::UpdateLight(int32_t a_lightIdId, pugi::xml_node a_lightNode)
{
  return true;
}

bool RD_HydraConnection::UpdateMesh(int32_t a_meshId, pugi::xml_node a_meshNode, const HRMeshDriverInput& a_input, const HRBatchInfo* a_batchList, int32_t listSize)
{
  return true;
}

bool RD_HydraConnection::UpdateMeshFromFile(int32_t a_meshId, pugi::xml_node a_meshNode, const wchar_t* a_fileName)
{
  return false;
}

bool RD_HydraConnection::UpdateCamera(pugi::xml_node a_camNode)
{
  return true;
}


bool RD_HydraConnection::UpdateSettings(pugi::xml_node a_settingsNode)
{
  m_width     = a_settingsNode.child(L"width").text().as_int();
  m_height    = a_settingsNode.child(L"height").text().as_int();

  m_enableMLT = (std::wstring(a_settingsNode.child(L"enable_mlt").text().as_string())       == L"1")   || 
                (std::wstring(a_settingsNode.child(L"method_secondary").text().as_string()) == L"mlt") || 
                (std::wstring(a_settingsNode.child(L"method_tertiary").text().as_string())  == L"mlt") ||
                (std::wstring(a_settingsNode.child(L"method_caustic").text().as_string())   == L"mlt");
 
  m_presets.errHDR  = a_settingsNode.child(L"pt_error").text().as_float()/100.0f;
  m_presets.minrays = a_settingsNode.child(L"minRaysPerPixel").text().as_int();
  m_presets.maxrays = a_settingsNode.child(L"maxRaysPerPixel").text().as_int();
  
  if (m_pColorBuffer == nullptr)
    m_pColorBuffer = std::make_shared<HDRImage4f>(m_width, m_height);
  else if (m_pColorBuffer->width() != m_width || m_pColorBuffer->height() != m_height)
    m_pColorBuffer->resize(m_width, m_height);

  m_drawTiles = (a_settingsNode.child(L"draw_tiles").text().as_int() == 1);

  return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::wstring GetAbsolutePath(const std::wstring& a_path);

void RD_HydraConnection::RunAllHydraHeads()
{
  int width  = m_width;
  int height = m_height;

  ////////////////////////////////////////////////////////////////////////////////////////////////////

  std::vector<int> devList;

  for (size_t i = 0; i < m_devList2.size(); i++)
  {
    if (m_devList2[i].isEnabled)
      devList.push_back(m_devList2[i].id);
  }

  if (m_devList2.size() == 0)
  {
    devList.resize(1);
    devList[0] = -1;
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////

  delete m_pConnection;
  m_pConnection = CreateHydraServerConnection(width, height, false, devList);

  if (m_pConnection != nullptr && m_pConnection->hasConnection())
  {
    RenderProcessRunParams params;

    params.compileShaders = false;
    params.debug = MODERN_DRIVER_DEBUG;
    params.enableMLT = m_enableMLT;
    params.normalPriorityCPU = false;
    params.showConsole = !m_hideCmd;

    const std::wstring libPath = GetAbsolutePath(m_libPath);
    std::string temp = ws2s(libPath);
    std::stringstream auxInput;
    auxInput << "-inputlib \"" << temp.c_str() << "\" -width " << width << " -height " << height << " ";

    params.customExePath = "C:/[Hydra]/bin2/hydra.exe";
    params.customExeArgs = auxInput.str();
    params.customLogFold = m_logFolderS;

    m_params = params;

    m_pConnection->runAllRenderProcesses(params, m_devList);
  }

}

void RD_HydraConnection::BeginScene()
{
  m_progressVal   = 0.0f;
  m_avgBrightness = 1.0f;
  m_avgBCounter   = 0;

  m_stopThreadImmediately = false;
  haveUpdateFromMT        = false;
  hadFinalUpdate          = false;

  // (3) run hydras
  //
  RunAllHydraHeads();

  m_firstUpdate = true;

  if (FB_UPDATE_MT)
  {
    m_threadFinished = false;
    m_threadIsRun = true;
    m_stopThreadImmediately = false;
    hadFinalUpdate = false;

    //std::async(std::launch::async, &RD_HydraConnection::UnionABImagesFunc, this);
    m_updateFbThread = std::move(std::thread(&RD_HydraConnection::UnionABImagesFunc, this));
    std::this_thread::yield();
  }

  m_instancesNum = 0;
}


void RD_HydraConnection::EndScene()
{
  
}

void RD_HydraConnection::EndFlush()
{
  ImageZ* pImageA = m_pConnection->imageA();
  if (pImageA != nullptr)
  {
    std::string firstMsg = "-node_t A -sid 0 -layer color -action start";
    if (!pImageA->SendMsg(firstMsg.c_str()))
      m_msg = L"RD_HydraConnection: ImageA can't send message";
  }
}

void RD_HydraConnection::Draw()
{
  // like glFinish();
}

void RD_HydraConnection::InstanceMeshes(int32_t a_meshId, const float* a_matrices, int32_t a_instNum, const int* a_lightInstId)
{
  m_instancesNum += a_instNum;
}

void RD_HydraConnection::InstanceLights(int32_t a_light_id, const float* a_matrix, pugi::xml_node* a_custAttrArray, int32_t a_instNum, int32_t a_lightGroupId)
{

}

HRRenderUpdateInfo RD_HydraConnection::HaveUpdateNow()
{
  HRRenderUpdateInfo result;

  // if (MODERN_DRIVER_DEBUG)
  // {
  //   result.finalUpdate  = true;
  //   result.haveUpdateFB = true;
  //   result.progress     = 0.0f;
  //   return result;
  // }

  if (m_pConnection == nullptr)
    return result;

  if (m_instancesNum == 0) // empty scene
  {
    result.finalUpdate  = true;
    result.haveUpdateFB = true;
    result.progress     = 100.0f;
    return result;
  }

  ImageZ* pImageA = m_pConnection->imageA();
  ImageZ* pImageB = m_pConnection->imageB();

  const bool notNull = (pImageA != nullptr) && (pImageB != nullptr);

  if (FB_UPDATE_MT)
    result.haveUpdateFB = haveUpdateFromMT;
  else
    result.haveUpdateFB = (notNull && !pImageB->IsEmpty());

  result.finalUpdate = (m_progressVal >= 99.9999f && result.haveUpdateFB) || (FB_UPDATE_MT && hadFinalUpdate);

  result.haveUpdateMSG = false;
  if (notNull)
  {
    std::string serverReply = pImageB->message();
    auto p = serverReply.find("[msg]");
    result.haveUpdateMSG = (p != std::string::npos);

    if (result.haveUpdateMSG)
    {
      std::wstring lastServerReply = s2ws(serverReply);
      wcsncpy(m_lastServerReply, lastServerReply.c_str(), 256);
      result.msg = m_lastServerReply;
    }
  }

  result.progress = m_progressVal;
  return result;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<float> LoadGBuffer(const std::wstring& a_folderPath, int* pW, int* pH);

static inline int   __float_as_int(float x) { return *((int*)&x); }
static inline float __int_as_float(int x) { return *((float*)&x); }

static inline int   as_int(float x) { return __float_as_int(x); }
static inline float as_float(int x) { return __int_as_float(x); }

static inline void decodeNormal(unsigned int a_data, float a_norm[3])
{
  const float divInv = 1.0f / 32767.0f;

  short a_enc_x, a_enc_y;

  a_enc_x = (short)(a_data & 0x0000FFFF);
  a_enc_y = (short)((int)(a_data & 0xFFFF0000) >> 16);

  float sign = (a_enc_x & 0x0001) ? -1.0f : 1.0f;

  a_norm[0] = (short)(a_enc_x & 0xfffe)*divInv;
  a_norm[1] = (short)(a_enc_y & 0xfffe)*divInv;
  a_norm[2] = sign*sqrt(fmax(1.0f - a_norm[0] * a_norm[0] - a_norm[1] * a_norm[1], 0.0f));
}

typedef HRGBufferPixel GBuffer1;

static inline GBuffer1 unpackGBuffer1(const float a_input[4])
{
  GBuffer1 res;

  res.depth = a_input[0];
  res.matId = as_int(a_input[2]);
  decodeNormal(as_int(a_input[1]), res.norm);

  unsigned int rgba = as_int(a_input[3]);
  res.rgba[0] = (rgba & 0x000000FF)*(1.0f / 255.0f);
  res.rgba[1] = ((rgba & 0x0000FF00) >> 8)*(1.0f / 255.0f);
  res.rgba[2] = ((rgba & 0x00FF0000) >> 16)*(1.0f / 255.0f);
  res.rgba[3] = ((rgba & 0xFF000000) >> 24)*(1.0f / 255.0f);

  return res;
}


static inline float unpackAlpha(float a_input[4])
{
  const unsigned int rgba = as_int(a_input[3]);
  return ((rgba & 0xFF000000) >> 24)*(1.0f / 255.0f);
}

static float ReadFloatParam(const char* message, const char* paramName)
{
  std::istringstream iss(message);

  float res = 0.0f;

  do
  {
    std::string name, val;
    iss >> name >> val;

    if (name == paramName)
    {
      res = (float)(atof(val.c_str()));
      break;
    }

  } while (iss);

  return res;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void RD_HydraConnection::GetFrameBufferColorFromABImages(int32_t w, int32_t h, float* a_out)
{
  if (m_pColorBuffer == nullptr)
  {
    m_msg = L"pColorBuffer == nullptr, render frame buffer was not initialized; perhaps resolution were not set?";
    return;
  }

  ImageZ* pImageA = m_pConnection->imageA();
  ImageZ* pImageB = m_pConnection->imageB();

  const bool notNull = (pImageA != nullptr) && (pImageB != nullptr);

  if (notNull && !pImageB->IsEmpty() && pImageB->Lock(100))
  {
    std::string serverReply = pImageB->message();

    if (serverReply.find("[msg]") != std::string::npos) // just a text message without image, works in the beggining only
    {
      std::wstring msgUnicode = s2ws(serverReply.substr(5, serverReply.size() - 5));

      auto p1 = msgUnicode.find_first_of(L"[");
      auto p2 = msgUnicode.find_first_of(L"%]");

      if (p1 != std::wstring::npos && p2 != std::wstring::npos)
      {
        std::wstring number = msgUnicode.substr(p1 + 1, p2 - p1 - 1);
        m_progressVal = (float)(_wtof(number.c_str()));
      }

    }
    else
    {
      pImageA->UnionWith(pImageB);

      m_progressVal = 100.0f*pImageA->EstimateProgress(m_presets.errHDR, m_presets.minrays, m_presets.maxrays); // pt sample pixels and tiles

      if (m_pColorBuffer->width() != w && m_pColorBuffer->height() != h)
        m_pColorBuffer->resize(w, h);

      float* data = m_pColorBuffer->data();

      if (m_params.enableMLT)
        CopyFromZMLT(w, h, data);
      else
        ZImageUtils::ImageZBlockMemToRowPitch(pImageA->ColorPtr(0), data, w, h);
      
      const int size = w*h*4;                                                                   // #TODO: fix that afterm implementation of alpha in new render driver
      for (int i = 0; i < size; i+=4)
      {
        a_out[i + 0] = data[i + 0];
        a_out[i + 1] = data[i + 1];
        a_out[i + 2] = data[i + 2];
        a_out[i + 3] = 1.0f; // m_fbDataHDR[i + 3];
      }
      //memcpy(a_out, &m_fbDataHDR[0], w*h * sizeof(float) * 4);

      //if (m_firstUpdate)
      //{
      //  int w = 0;
      //  int h = 0;
      //
      //  m_gbuffer = LoadGBuffer(L"C:/[Hydra]/temp/filter/gbuffer1.image4f", &w, &h);
      //
      //  for (size_t i = 0; i < m_gbuffer.size(); i += 4)
      //  {
      //    float* data = &m_gbuffer[i];
      //    a_out[i + 3] = unpackAlpha(data);
      //  }
      //
      //  m_firstUpdate = false;
      //}
    }

    pImageB->Clear();
    pImageB->Unlock();

  }
  else if (m_pColorBuffer != nullptr)
  {
    if (m_pColorBuffer->width() == w && m_pColorBuffer->height() == h)
    {
      const float* data = m_pColorBuffer->data();
      memcpy(a_out, data, w*h * sizeof(float) * 4);
    }
  }

}


static inline float contribFunc(double color[4])
{
  return (float)(fmax(0.33334f*(color[0] + color[1] + color[2]), 0.0));
}

void RD_HydraConnection::CopyFromZMLT(int32_t w, int32_t h, float* a_data)
{
  struct float4 { float x, y, z, w; };

  ImageZ* pInput = m_pConnection->imageA();

  int zimageWidth = pInput->Width();
  int zimageHeight = pInput->Height();

  if (zimageWidth != m_width || zimageHeight != m_height)
    return;

  float* colorP = pInput->ColorPtr(0);
  float* colorS = pInput->ColorPtr(1);

  if (colorP == nullptr || colorS == nullptr || a_data == nullptr)
    return;

  float4* colorP2 = (float4*)colorP;
  float4* colorS2 = (float4*)colorS;

  double avgB4[4] = { 0, 0, 0, 0 };

  for (int y = 0; y < m_height; y++)
  {
    for (int x = 0; x < m_width; x++)
    {
      int indexSrc = ZImageUtils::IndexZB(x, y, zimageWidth);
      float4 col = colorS2[indexSrc];

      if (isfinite(col.x)) avgB4[0] += col.x;
      if (isfinite(col.y)) avgB4[1] += col.y;
      if (isfinite(col.z)) avgB4[2] += col.z;
      if (isfinite(col.w)) avgB4[3] += col.w;
    }
  }

  avgB4[0] *= (1.0 / double(m_width*m_height));
  avgB4[1] *= (1.0 / double(m_width*m_height));
  avgB4[2] *= (1.0 / double(m_width*m_height));
  avgB4[3] *= (1.0 / double(m_width*m_height));

  const double avgbPlugin = contribFunc(avgB4);

  const float normConst = (float)(double(m_avgBrightness) / avgbPlugin);

  // median filter secondary light
  //
  if (m_tempImage.width() != m_width || m_tempImage.height() != m_height)
    m_tempImage.resize(m_width, m_height);

  float4* pImgData = (float4*)m_tempImage.data();

  for (int y = 0; y < m_height; y++)
  {
    for (int x = 0; x < m_width; x++)
    {
      int indexSrc = ZImageUtils::IndexZB(x, y, zimageWidth);
      int index1 = y*m_width + x;

      float4 indirect = colorS2[indexSrc];
      indirect.x *= normConst;
      indirect.y *= normConst;
      indirect.z *= normConst;
      indirect.w *= normConst;
      pImgData[index1] = indirect;
    }
  }

  if (m_enableMedianFilter)
    m_tempImage.medianFilterInPlace(m_medianthreshold, m_avgBrightness);

  // final pass, get image
  //
  for (int y = 0; y < m_height; y++)
  {
    for (int x = 0; x < m_width; x++)
    {
      int indexSrc = ZImageUtils::IndexZB(x, y, zimageWidth);

      float4 primary = colorP2[indexSrc];
      float4 secondary = colorS2[indexSrc];

      int index1 = y*m_width + x;
      float4 secondary2 = pImgData[index1];

      a_data[index1 * 4 + 0] = primary.x + secondary2.x;
      a_data[index1 * 4 + 1] = primary.y + secondary2.y;
      a_data[index1 * 4 + 2] = primary.z + secondary2.z;
      //a_data[index1 * 4 + 3] = primary.w + secondary2.w;
    }
  }

}


void RD_HydraConnection::GetFrameBufferColorFromABImagesMLT(int32_t w, int32_t h, float* a_out)
{
  ImageZ* pImageA = m_pConnection->imageA();
  ImageZ* pImageB = m_pConnection->imageB();

  const bool notNull = (pImageA != nullptr) && (pImageB != nullptr);

  if (notNull && !pImageB->IsEmpty() && pImageB->Lock(100))
  {
    std::string serverReply = pImageB->message();

    if (serverReply.find("[msg]") != std::string::npos) // just a text message without image, works in the beggining only
    {
      std::wstring msgUnicode = s2ws(serverReply.substr(5, serverReply.size() - 5));

      auto p1 = msgUnicode.find_first_of(L"[");
      auto p2 = msgUnicode.find_first_of(L"%]");

      if (p1 != std::wstring::npos && p2 != std::wstring::npos)
      {
        std::wstring number = msgUnicode.substr(p1 + 1, p2 - p1 - 1);
        m_progressVal = (float)(_wtof(number.c_str()));
      }

    }
    else
    {
      pImageA->UnionWith(pImageB);

      float avgBrightness = ReadFloatParam(pImageB->message(), "-avgb");
      float alpha = 1.0f / float(m_avgBCounter + 1);
      m_avgBrightness = avgBrightness*alpha + m_avgBrightness*(1.0f - alpha);

      m_progressVal = 100.0f*pImageA->EstimateProgress(0.0f, m_presets.maxrays, m_presets.maxrays); // mlt sample whole image

      CopyFromZMLT(w, h, a_out);

      //if (m_firstUpdate)
      //{
      //  int w = 0;
      //  int h = 0;
      //
      //  m_gbuffer = LoadGBuffer(L"C:/[Hydra]/temp/filter/gbuffer1.image4f", &w, &h);
      //
      //  for (size_t i = 0; i < m_gbuffer.size(); i += 4)
      //  {
      //    float* data = &m_gbuffer[i];
      //    a_out[i + 3] = unpackAlpha(data);
      //  }
      //
      //  m_firstUpdate = false;
      //}
    }

    pImageB->Clear();
    pImageB->Unlock();

  }
}


void RD_HydraConnection::UnionABImagesFunc()
{
  ImageZ* pImageA = m_pConnection->imageA();
  ImageZ* pImageB = m_pConnection->imageB();

  while (true)
  {
    const bool notNull = (pImageA != nullptr) && (pImageB != nullptr);
    const bool haveUpdateFB = (notNull && !pImageB->IsEmpty());

    bool finalUpdate = false;

    if (haveUpdateFB)
    {
      ImageZ* pImageA = m_pConnection->imageA();
      ImageZ* pImageB = m_pConnection->imageB();

      const bool notNull = (pImageA != nullptr) && (pImageB != nullptr);

      if (notNull && !pImageB->IsEmpty() && pImageB->Lock(100))
      {
        std::string serverReply = pImageB->message();

        if (serverReply.find("[msg]") != std::string::npos) // just a text message without image, works in the beggining only
        {
          std::wstring msgUnicode = s2ws(serverReply.substr(5, serverReply.size() - 5));

          auto p1 = msgUnicode.find_first_of(L"[");
          auto p2 = msgUnicode.find_first_of(L"%]");

          if (p1 != std::wstring::npos && p2 != std::wstring::npos)
          {
            std::wstring number = msgUnicode.substr(p1 + 1, p2 - p1 - 1);
            m_progressVal = (float)(_wtof(number.c_str()));
          }

        }
        else
        {
          m_abMutex.lock();

          pImageA->UnionWith(pImageB);

          if (m_params.enableMLT)
          {
            float avgBrightness = ReadFloatParam(pImageB->message(), "-avgb");
            float alpha = 1.0f / float(m_avgBCounter + 1);
            m_avgBrightness = avgBrightness*alpha + m_avgBrightness*(1.0f - alpha);
          }

          m_progressVal = 100.0f*pImageA->EstimateProgress(m_presets.errHDR, m_presets.minrays, m_presets.maxrays); // pt sample pixels and tiles
          finalUpdate   = (m_progressVal >= 99.9999f) && haveUpdateFB;

          m_abMutex.unlock();

          haveUpdateFromMT = true;
          if (finalUpdate)
            hadFinalUpdate = true;
          // std::cout << "async framebuffer Update ... " << std::endl;
        }

        pImageB->Clear();
        pImageB->Unlock();

      }
    }

    if (finalUpdate || m_stopThreadImmediately) // info.finalUpdate
      break;

    std::this_thread::sleep_for(std::chrono::microseconds(250));
  }

  //if (m_stopThreadImmediately)
  //{
  std::stringstream sout2;
  sout2 << "-node_t A" << " -sid " << 0 << " -layer color " << " -action exitnow";
  pImageA->SendMsg(sout2.str().c_str());
  //}

  m_threadIsRun = false;
  m_threadFinished = true;
}


void RD_HydraConnection::GetFrameBufferHDR(int32_t w, int32_t h, float* a_out, const wchar_t* a_layerName)
{
  if (FB_UPDATE_MT)
  {
    m_abMutex.lock();

    ImageZ* pImageA = m_pConnection->imageA();

    if (m_params.enableMLT)
      CopyFromZMLT(w, h, a_out);
    else
      ZImageUtils::ImageZBlockMemToRowPitch(pImageA->ColorPtr(0), a_out, w, h);

    m_abMutex.unlock();

    haveUpdateFromMT = false;

    if (m_firstUpdate)
    {
      int w = 0;
      int h = 0;

      m_gbuffer = LoadGBuffer(L"C:/[Hydra]/temp/filter/gbuffer1.image4f", &w, &h);

      for (size_t i = 0; i < m_gbuffer.size(); i += 4)
      {
        float* data = &m_gbuffer[i];
        a_out[i + 3] = unpackAlpha(data);
      }

      m_firstUpdate = false;
    }
  }
  else
  {
    std::wstring layer(a_layerName);
    if (layer == L"color" || layer == L"clr")
    {
      if (m_params.enableMLT)
        GetFrameBufferColorFromABImagesMLT(w, h, a_out);
      else
        GetFrameBufferColorFromABImages(w, h, a_out);
    }
  }

}


static inline float clamp(float u, float a, float b) { float r = fmax(a, u); return fmin(r, b); }
static inline int   clamp(int u, int a, int b) { int r = (a > u) ? a : u; return (r < b) ? r : b; }

static inline int RealColorToUint32(const float real_color[4])
{
  float  r = clamp(real_color[0] * 255.0f, 0.0f, 255.0f);
  float  g = clamp(real_color[1] * 255.0f, 0.0f, 255.0f);
  float  b = clamp(real_color[2] * 255.0f, 0.0f, 255.0f);
  float  a = clamp(real_color[3] * 255.0f, 0.0f, 255.0f);

  unsigned char red = (unsigned char)r;
  unsigned char green = (unsigned char)g;
  unsigned char blue = (unsigned char)b;
  unsigned char alpha = (unsigned char)a;

  return red | (green << 8) | (blue << 16) | (alpha << 24);
}

void RD_HydraConnection::GetFrameBufferLDR(int32_t w, int32_t h, int32_t* a_out)
{
  if (m_pColorBuffer == nullptr)
  {
    m_msg = L"pColorBuffer == nullptr, render frame buffer was not initialized; perhaps resolution were not set?";
    return;
  }

  if (m_pColorBuffer->width() != w && m_pColorBuffer->height() != h)
    m_pColorBuffer->resize(w, h);

  float* data = m_pColorBuffer->data();

  GetFrameBufferHDR(w, h, data, L"color");

  const float invGamma = 1.0f / 2.2f;

  #pragma omp parallel for
  for (int i = 0; i < w*h; i++)
  {
    const float r = data[i * 4 + 0];
    const float g = data[i * 4 + 1];
    const float b = data[i * 4 + 2];
    const float a = data[i * 4 + 3];

    const float color[4] = { powf(r, invGamma),
                             powf(g, invGamma),
                             powf(b, invGamma),
                             a
    };

    a_out[i] = RealColorToUint32(color);
  }

  // draw red tiles over image if this is enabled
  //
  ImageZ* pImageA = m_pConnection->imageA();
 
  if (m_drawTiles && pImageA != nullptr) 
  {
    const ZBlockT2* zblocks = pImageA->ZBlocksPtr();
    const int lineColor = 0x000000FF;

    int bx = 0, by = 0;
    for (int z = 0; z < pImageA->ZBlocksNum(); z++)
    {
      if (zblocks[z].index2 == 0xFFFFFFFF)
      {
        bx += 16;
        if (bx >= m_width)
        {
          bx = 0;
          by += 16;
        }
        continue;
      }

      // draw tile at (bx,by)
      //

      const int oy = (by + 16 >= m_height) ? 15 : 16;
      const int ox = (bx + 16 >= m_width)  ? 15 : 16;

      for (int x = 0; x < 16; x++)
      {
        a_out[(by + 0 )*m_width + bx + x] = lineColor;
        a_out[(by + oy)*m_width + bx + x] = lineColor;
      }

      for (int y = 0; y < 16; y++)
      {
        a_out[(by + y)*m_width + bx + 0 ] = lineColor;
        a_out[(by + y)*m_width + bx + ox] = lineColor;
      }

      bx += 16;
      if (bx >= m_width)
      {
        bx = 0;
        by += 16;
      }
    }
    
  } 


}

void RD_HydraConnection::GetGBufferLine(int32_t a_lineNumber, HRGBufferPixel* a_lineData, int32_t a_startX, int32_t a_endX)
{
  if (a_endX > m_width)
    a_endX = m_width;

  const int32_t lineOffset = (a_lineNumber*m_width + a_startX);
  const int32_t lineSize = (a_endX - a_startX);

  for (int32_t x = 0; x < lineSize; x++)
  {
    const float* data = &m_gbuffer[(lineOffset + x) * 4];
    a_lineData[x] = unpackGBuffer1(data);
  }

}

void RD_HydraConnection::ExecuteCommand(const wchar_t* a_cmd, wchar_t* a_out)
{
  std::string inputA = ws2s(a_cmd);

  std::wstringstream inputStream(a_cmd);
  std::wstring name, value;
  inputStream >> name >> value;

  if (name == L"pause")
  {
    if (m_pConnection == nullptr)
      return;

    ImageZ* pImageA = m_pConnection->imageA();
    if (pImageA == nullptr)
      return;

    // (1) save imageA
    //
    pImageA->SaveToFile(value.c_str());

    // (2) finish all render processes
    //
    inputA = "exitnow";
  }
  else if (name == L"resume")
  {
    // (1) create connection and run process
    //
    RunAllHydraHeads();

    // (2) load imageA
    //
    ImageZ* pImageA = m_pConnection->imageA();
    pImageA->LoadFromFile(value.c_str());

    inputA = "start";
  }

  if (m_pConnection == nullptr)
    return;

  ImageZ* pImageA = m_pConnection->imageA();
  if (pImageA == nullptr)
    return;

  std::stringstream sout2;
  sout2 << "-node_t A" << " -sid 0 -layer color -action " << inputA.c_str();
  
  if (!pImageA->SendMsg(sout2.str().c_str()))
    m_msg = L"can't finish render due to connection->imageA() is busy or invalid ... ";
  

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

IHRRenderDriver* CreateHydraConnection_RenderDriver()
{
  return new RD_HydraConnection;
}



