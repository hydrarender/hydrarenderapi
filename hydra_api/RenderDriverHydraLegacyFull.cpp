#include "HydraRenderDriverAPI.h"

#include <vector>
#include <string>
#include <sstream>
#include <fstream>

#include "HydraInternal.h" // #TODO: this is only for hr_mkdir and hr_cleardir. Remove this further
#include "RenderDriverHydraLegacyStuff.h"

#include "HydraXMLHelpers.h"

#include "HR_HDRImage.h"

#include <mutex>
#include <future>
#include <thread>

#pragma warning(disable:4996) // for wcscpy to be ok

static const bool FB_UPDATE_MT = false; // #TODO: figure out thing around std:;slepp_for, thread and async - they are not so simple.

using HydraRender::HDRImage4f;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct SimpleMesh
{
 
  SimpleMesh() {}
  SimpleMesh(SimpleMesh&& rhs) = default;

  SimpleMesh(const SimpleMesh& rhs)            = default;
  SimpleMesh& operator=(const SimpleMesh& rhs) = default;
  SimpleMesh& operator=(SimpleMesh&& rhs)      = default;

  void clear()
  {
    verticesPos.clear();
    verticesNorm.clear();
    verticesTexCoord.clear();
    triIndices.clear();
    matIndices.clear();
  }

  void freeMem()
  {
    verticesPos = std::vector<float>();
    verticesNorm = std::vector<float>();
    verticesTexCoord = std::vector<float>();
    triIndices = std::vector<int32_t>();
    matIndices = std::vector<int32_t>();
  }

  void reserve(int vNum, int indNum)
  {
    verticesPos.reserve(vNum * 4 + 10);
    verticesNorm.reserve(vNum * 4 + 10);
    verticesTexCoord.reserve(vNum * 2 + 10);
    triIndices.reserve(indNum + 10);
    matIndices.reserve(indNum / 3 + 10);
  }

  std::vector<float>    verticesPos;       ///< float4
  std::vector<float>    verticesNorm;      ///< float4
  std::vector<float>    verticesTexCoord;  ///< float2
  std::vector<int32_t>  triIndices;        ///< size of 3*triNum
  std::vector<int32_t>  matIndices;        ///< size of 1*triNum
};


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct RD_HydraLegacy : public IHRRenderDriver
{
  RD_HydraLegacy() : m_pConnection(nullptr), m_progressVal(0.0f), m_firstUpdate(true), m_width(0), m_height(0), m_avgBrightness(0.0f), m_avgBCounter(0),
                     m_enableMedianFilter(false), m_medianthreshold(0.4f), m_stopThreadImmediately(false), haveUpdateFromMT(false), m_threadIsRun(false), m_threadFinished(false), hadFinalUpdate(false)
  {
    m_msg = L"";
    InitBothDeviceList();
  }

  ~RD_HydraLegacy()
  {
    ClearAll();
  }

  void              ClearAll();
  HRDriverAllocInfo AllocAll(HRDriverAllocInfo a_info);

  void GetLastErrorW(wchar_t a_msg[256]);

  bool UpdateImage(int32_t a_texId, int32_t w, int32_t h, int32_t bpp, const void* a_data, pugi::xml_node a_texNode);
  bool UpdateMaterial(int32_t a_matId, pugi::xml_node a_materialNode);
  void UpdateTextureSamplers(pugi::xml_node a_node);
  void ValAttributeToText(pugi::xml_node a_node);
  bool UpdateLight(int32_t a_lightIdId, pugi::xml_node a_lightNode);
  bool UpdateMesh(int32_t a_meshId, pugi::xml_node a_meshNode, const HRMeshDriverInput& a_input, const HRBatchInfo* a_batchList, int32_t listSize);

  bool UpdateImageFromFile(int32_t a_texId, const wchar_t* a_fileName, pugi::xml_node a_texNode);
  bool UpdateMeshFromFile(int32_t a_meshId, pugi::xml_node a_meshNode, const wchar_t* a_fileName);

  bool UpdateCamera(pugi::xml_node a_camNode);
  bool UpdateSettings(pugi::xml_node a_settingsNode);

  /////////////////////////////////////////////////////////////////////////////////////////////

  void BeginScene();
  void EndScene();
  void InstanceMeshes(int32_t a_mesh_id, const float* a_matrices, int32_t a_instNum, const int* a_lightInstId) override;
  void InstanceLights(int32_t a_light_id, const float* a_matrix, pugi::xml_node* a_custAttrArray, int32_t a_instNum, int32_t a_lightGroupId) override;

  void Draw();

  HRRenderUpdateInfo HaveUpdateNow();

  void GetFrameBufferHDR(int32_t w, int32_t h, float*   a_out, const wchar_t* a_layerName);
  void GetFrameBufferLDR(int32_t w, int32_t h, int32_t* a_out);

  void GetGBufferLine(int32_t a_lineNumber, HRGBufferPixel* a_lineData, int32_t a_startX, int32_t a_endX);
  
  // info and devices
  //
  HRDriverInfo Info();
  const HRRenderDeviceInfoListElem* DeviceList() const override;
  void EnableDevice(int32_t id, bool a_enable);

  void ExecuteCommand(const wchar_t* a_cmd, wchar_t* a_out);

protected:

  std::wstring m_libPath;
  std::wstring m_msg;
  std::vector<SimpleMesh> m_meshes;
  SimpleMesh              m_wholeScene;

  pugi::xml_document      m_docLib;
  pugi::xml_node          m_lightsLib;
  pugi::xml_node          m_materialsLib;
  pugi::xml_node          m_texturesLib;

  pugi::xml_document      m_doc;
  pugi::xml_node          m_materialsInst;
  pugi::xml_node          m_lightsInst;
  pugi::xml_node          m_camerasInst;

  pugi::xml_document      m_docSettings;
  pugi::xml_node          m_settings;

  void ScanAndFixTextures(pugi::xml_node a_matNode);
  void ScanAndFixIes(pugi::xml_node a_node);
  void InstanceAllMaterials(pugi::xml_node a_lib, pugi::xml_node a_inst);
  void InitBothDeviceList();

  void GetFrameBufferColorFromABImages(int32_t w, int32_t h, float* a_out);
  void GetFrameBufferColorFromABImagesMLT(int32_t w, int32_t h, float* a_out);
  void UnionABImagesFunc();

  void CopyFromZMLT(int32_t w, int32_t h, float* a_out);

  int m_currLightIsntanceId;

  HRDriverAllocInfo m_currAllocInfo;

  // legacy crap
  //
  IHydraNetPluginAPI* m_pConnection;

  std::vector<HydaRenderDevice>           m_devList;
  std::vector<HRRenderDeviceInfoListElem> m_devList2;

  std::vector<float> m_fbDataHDR;
  std::vector<float> m_gbuffer;
  float m_progressVal;
  RenderProcessRunParams m_params;

  bool m_haveSkyLight = false;

  struct RenderPresets
  {
    int   minrays;
    int   maxrays;
    float errHDR;
  } m_presets;

  bool m_firstUpdate;

  int m_width;
  int m_height;

  float m_avgBrightness;
  int   m_avgBCounter;

  HDRImage4f m_tempImage;

  bool  m_enableMedianFilter;
  float m_medianthreshold;

  bool m_stopThreadImmediately;
  bool m_threadIsRun;
  bool m_threadFinished;
  std::mutex m_abMutex;
  bool haveUpdateFromMT;
  bool hadFinalUpdate;
  wchar_t m_lastServerReply[256];

  std::thread m_updateFbThread;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static void clear_node_childs2(pugi::xml_node a_xmlNode)
{
  for (pugi::xml_node child = a_xmlNode.first_child(); child;)
  {
    pugi::xml_node next = child.next_sibling();
    child.parent().remove_child(child);
    child = next;
  }
}

static void clear_node2(pugi::xml_node a_xmlNode)
{
  // clear all attribures
  //
  for (pugi::xml_attribute attr = a_xmlNode.first_attribute(); attr;)
  {
    pugi::xml_attribute next = attr.next_attribute();
    a_xmlNode.remove_attribute(attr);
    attr = next;
  }

  clear_node_childs2(a_xmlNode);
}

static pugi::xml_node force_child(pugi::xml_node a_parent, const wchar_t* a_name) ///< helper function
{
  pugi::xml_node child = a_parent.child(a_name);
  if (child != nullptr)
    return child;
  else
    return a_parent.append_child(a_name);
}

static pugi::xml_attribute force_attrib(pugi::xml_node a_parent, const wchar_t* a_name) ///< helper function
{
  pugi::xml_attribute attr = a_parent.attribute(a_name);
  if (attr != nullptr)
    return attr;
  else
    return a_parent.append_attribute(a_name);
}


std::wstring GetAbsolutePath(const std::wstring& a_path)
{
  if (a_path == L"")
    return L"";

  //std::wstring abs_path_str = L"";
  std::wstring path = a_path;

  if (path.size() > 8 && path.substr(0, 8) == L"file:///")
    path = path.substr(8, path.size());

  else if (path.size() > 7 && path.substr(0, 7) == L"file://")
    path = path.substr(7, path.size());

  for (int i = 0; i<path.size(); i++)
  {
    if (path[i] == (L"\\")[0])
      path[i] = (L"/")[0];
  }

#ifdef WIN32

  wchar_t buffer[4096];
  GetFullPathNameW(path.c_str(), 4096, buffer, NULL);
  return std::wstring(buffer);

#else

  boost::filesystem::path bst_path(path);
  boost::filesystem::path abs_path = system_complete(bst_path);
  return abs_path.string();

#endif


}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void RD_HydraLegacy::InitBothDeviceList()
{
  m_devList = InitDeviceList(1);
  m_devList2.resize(m_devList.size());

  // form list in memory ... )
  //
  for (size_t i = 0; i < m_devList2.size(); i++)
  {
    const HydaRenderDevice& devInfo       = m_devList[i];
    HRRenderDeviceInfoListElem* pListElem = &m_devList2[i];

    pListElem->id        = (int32_t)(i);
    pListElem->isCPU     = devInfo.isCPU;
    pListElem->isEnabled = false;

    wcscpy(pListElem->name, devInfo.name.c_str());
    wcscpy(pListElem->driver, devInfo.driverName.c_str());

    if (i != m_devList2.size() - 1)
      pListElem->next = &m_devList2[i + 1];
    else
      pListElem->next = nullptr;
  }
}

void RD_HydraLegacy::ClearAll()
{
  if (FB_UPDATE_MT)
  {
    while (!m_threadFinished)
    {
      m_stopThreadImmediately = true;
      std::this_thread::sleep_for(std::chrono::microseconds(10));
    }
  }

  clear_node2(m_doc);
  clear_node2(m_docLib);

  delete m_pConnection;
  m_pConnection = nullptr;
}

HRDriverAllocInfo RD_HydraLegacy::AllocAll(HRDriverAllocInfo a_info)
{
  m_meshes.resize(a_info.geomNum);

  for (size_t i = 0; i < m_meshes.size(); i++)
    m_meshes[i].clear();

  pugi::xml_node collada = m_doc.append_child(L"COLLADA");
  
  collada.append_attribute(L"profile").set_value(L"HydraProfile");

  m_materialsInst = collada.append_child(L"library_materials");
  m_lightsInst    = collada.append_child(L"library_lights");
  m_camerasInst   = collada.append_child(L"library_cameras");

  m_lightsLib     = m_docLib.append_child(L"library_lights");
  m_materialsLib  = m_docLib.append_child(L"library_materials");
  m_texturesLib   = m_docLib.append_child(L"library_textures");

  m_settings = m_docSettings.append_child(L"hydra_presets");
  m_libPath  = std::wstring(a_info.libraryPath);

  m_currAllocInfo = a_info;

  return m_currAllocInfo;
}

const HRRenderDeviceInfoListElem* RD_HydraLegacy::DeviceList() const
{
  if (m_devList2.size() == 0)
    return nullptr;
  else
    return &m_devList2[0];
}

void RD_HydraLegacy::EnableDevice(int32_t id, bool a_enable)
{
  if (id < m_devList2.size())
    m_devList2[id].isEnabled = a_enable;
}


HRDriverInfo RD_HydraLegacy::Info()
{
  HRDriverInfo info; 

  info.supportHDRFrameBuffer              = true;
  info.supportHDRTextures                 = true;
  info.supportMultiMaterialInstance       = false;

  info.supportImageLoadFromInternalFormat = false;
  info.supportMeshLoadFromInternalFormat  = false;

  info.supportImageLoadFromExternalFormat = true;
  info.supportLighting                    = true;
  info.createsLightGeometryItself         = !LEGACY_DRIVER_DEBUG;

  info.memTotal                           = int64_t(8) * int64_t(1024 * 1024 * 1024);

  return info;
}

void RD_HydraLegacy::GetLastErrorW(wchar_t a_msg[256])
{
  wcscpy(a_msg, m_msg.c_str());
}


bool RD_HydraLegacy::UpdateImage(int32_t a_texId, int32_t w, int32_t h, int32_t bpp, const void* a_data, pugi::xml_node a_texNode)
{
  std::wstring path1 = m_libPath + std::wstring(L"/") + a_texNode.attribute(L"loc").as_string();
  std::wstring path2 = GetAbsolutePath(path1);

  std::wstringstream strout;
  strout << a_texId;
  std::wstring strId = strout.str();

  pugi::xml_node texNode = m_texturesLib.find_child_by_attribute(L"id", strId.c_str());

  if (texNode == nullptr)
  {
    texNode = m_texturesLib.append_child(L"texture");
    texNode.append_attribute(L"id").set_value(strId.c_str());
  }

  force_attrib(texNode, L"name").set_value(path2.c_str());

  return false;
}

bool RD_HydraLegacy::UpdateImageFromFile(int32_t a_texId, const wchar_t* a_fileName, pugi::xml_node a_texNode)
{
  std::wstringstream strout;
  strout << a_texId;
  std::wstring strId = strout.str();
  
  pugi::xml_node texNode = m_texturesLib.find_child_by_attribute(L"id", strId.c_str());
  
  if (texNode == nullptr)
  {
    texNode = m_texturesLib.append_child(L"texture");
    texNode.append_attribute(L"id").set_value(strId.c_str());
  }

  force_attrib(texNode, L"name").set_value(a_fileName);

  return true;
}

void RD_HydraLegacy::UpdateTextureSamplers(pugi::xml_node a_node)
{
  if (std::wstring(a_node.name()) == L"texture")
  {

    pugi::xml_node matSamplerNode;

    pugi::xml_node component = a_node.parent().parent();


    if (std::wstring(a_node.parent().name()) == L"glossiness")
      matSamplerNode = component.append_child(L"glossiness_sampler");
    else if (std::wstring(a_node.parent().name()) == L"opacity")
    {
      if (component.child(L"transparency") == nullptr)
        component.append_child(L"transparency");

      matSamplerNode = component.child(L"transparency").append_child(L"opacity_sampler");
    }
    else if(std::wstring(a_node.parent().name()) == L"mask")
    {
      matSamplerNode = component.append_child(L"mask_sampler");
    }
    else
      matSamplerNode = component.append_child(L"sampler");

    matSamplerNode.append_child(L"matrix").text().set(a_node.attribute(L"matrix").as_string());
    matSamplerNode.append_child(L"addressing_mode_u").text().set(a_node.attribute(L"addressing_mode_u").as_string());
    matSamplerNode.append_child(L"addressing_mode_v").text().set(a_node.attribute(L"addressing_mode_v").as_string());
    matSamplerNode.append_child(L"input_gamma").text().set(a_node.attribute(L"input_gamma").as_string());
    matSamplerNode.append_child(L"input_alpha").text().set(a_node.attribute(L"input_alpha").as_string());

    //a_node.text().set(texPath);
    
  }
  else
  {
    for (auto child = a_node.first_child(); child != nullptr; child = child.next_sibling())
      UpdateTextureSamplers(child);
  }

}

void RD_HydraLegacy::ValAttributeToText(pugi::xml_node a_node)
{
  std::wstring node_name(a_node.name());

  if (std::wstring(a_node.attribute(L"val").as_string()) != L"" || node_name.compare(std::wstring(L"height_map")) == 0 || node_name.compare(std::wstring(L"normal_map")) == 0)
  {
    a_node.text().set(a_node.attribute(L"val").as_string());

    auto tex = a_node.child(L"texture");

    if (tex != nullptr)
    {
      auto tex_id = tex.attribute(L"id").as_string();
      pugi::xml_node texNode = m_texturesLib.find_child_by_attribute(L"id", tex_id);

      auto tex_path = texNode.attribute(L"name").as_string();

      if(node_name.compare(std::wstring(L"glossiness")) == 0)
      {
        a_node.parent().append_child(L"glossiness_texture").text().set(tex_path);
        a_node.parent().child(L"glossiness_texture").append_attribute(L"id").set_value(tex_id);
      }
      else if (node_name.compare(std::wstring(L"height_map")) == 0)
      { // 
        a_node.parent().append_child(L"height_texture").text().set(tex_path);
        a_node.parent().child(L"height_texture").append_attribute(L"id").set_value(tex_id);
      }
      else if (node_name.compare(std::wstring(L"normal_map")) == 0)
      { // 
        a_node.parent().append_child(L"normals_texture").text().set(tex_path);
        a_node.parent().child(L"normals_texture").append_attribute(L"id").set_value(tex_id);
      }
      else if (node_name.compare(std::wstring(L"mask")) == 0)
      {
        a_node.text().set(tex_path);
        a_node.parent().append_child(L"mask_mult").text().set(a_node.attribute(L"val").as_string());
      }
      else
      {
        a_node.parent().append_child(L"texture").text().set(tex_path);
        a_node.parent().child(L"texture").append_attribute(L"id").set_value(tex_id);
      }
      a_node.remove_child(L"texture");
    }
  }
  else if(node_name.compare(std::wstring(L"opacity")) == 0)
  {
    auto tex = a_node.child(L"texture");

    if (tex != nullptr)
    {
      auto tex_id = tex.attribute(L"id").as_string();
      pugi::xml_node texNode = m_texturesLib.find_child_by_attribute(L"id", tex_id);

      auto tex_path = texNode.attribute(L"name").as_string();

      std::wstring node_name(a_node.name());

      auto transp =  a_node.parent().child(L"transparency");

      if(transp == nullptr)
        transp = a_node.parent().append_child(L"transparency");

      transp.append_child(L"opacity_texture").text().set(tex_path);
      transp.child(L"opacity_texture").append_attribute(L"id").set_value(tex_id);
    }
  }
  else
  {
    for (auto child = a_node.first_child(); child != nullptr; child = child.next_sibling())
      ValAttributeToText(child);
  }

}

inline static int find_interval(float x)
{
  if (fabs(x - 1.0f) < 1e-5f)
    return 10;
  else
    return (int)(x * 10);
}

inline static float GetCosPowerFromMaxShiness(float glosiness)
{
  float cMin = 1.0f;
  float cMax = 1000000.0f;

  float x = glosiness;

  float coeff[10][4] = {
    { 8.88178419700125e-14f, -1.77635683940025e-14f, 5.0f, 1.0f }, //0-0.1
    { 357.142857142857f, -35.7142857142857f, 5.0f, 1.5f }, //0.1-0.2
    { -2142.85714285714f, 428.571428571429f, 8.57142857142857f, 2.0f }, //0.2-0.3
    { 428.571428571431f, -42.8571428571432f, 30.0f, 5.0f }, //0.3-0.4
    { 2095.23809523810f, -152.380952380952f, 34.2857142857143f, 8.0f }, //0.4-0.5
    { -4761.90476190476f, 1809.52380952381f, 66.6666666666667f, 12.0f },//0.5-0.6
    { 9914.71215351811f, 1151.38592750533f, 285.714285714286f, 32.0f }, //0.6-0.7
    { 45037.7068059246f, 9161.90096119855f, 813.432835820895f, 82.0f }, //0.7-0.8
    { 167903.678757035f, 183240.189801913f, 3996.94423223835f, 300.0f }, //0.8-0.9
    { -20281790.7444668f, 6301358.14889336f, 45682.0925553320f, 2700.0f } //0.9-1.0
  };

  int k = find_interval(x);

  if (k == 10 || x >= 0.99f)
    return cMax;
  else
    return coeff[k][3] + coeff[k][2] * (x - k*0.1f) + coeff[k][1] * powf((x - k*0.1f), 2.0f) + coeff[k][0] * powf((x - k*0.1f), 3.0f);
}

bool RD_HydraLegacy::UpdateMaterial(int32_t a_matId, pugi::xml_node a_materialNode)
{
	std::wstring mtype = a_materialNode.attribute(L"type").as_string();
	//std::wstring mname = a_materialNode.attribute(L"name").as_string();

  std::wstringstream strout;
  strout << a_matId;
  std::wstring strId = strout.str();
  
  pugi::xml_node nodeToCopy = m_materialsLib.find_child_by_attribute(L"id", strId.c_str());
  pugi::xml_node newNode;

  if (nodeToCopy != nullptr)
  {
    newNode = m_materialsLib.insert_copy_after(a_materialNode, nodeToCopy);
    m_materialsLib.remove_child(nodeToCopy);
  }
  else
    newNode = m_materialsLib.append_copy(a_materialNode);

	if (mtype == L"shadow_catcher")
		newNode.attribute(L"type").set_value(L"hydra_material");


  if(mtype == L"hydra_blend")
  {
    newNode.force_child(L"node_top").force_attribute(L"type").set_value(L"material_id");
    newNode.child(L"node_top").text() = newNode.attribute(L"node_top").as_string();

    newNode.force_child(L"node_bottom").force_attribute(L"type").set_value(L"material_id");
    newNode.child(L"node_bottom").text() = newNode.attribute(L"node_bottom").as_string();
  }

  pugi::xml_node emissionMult = a_materialNode.child(L"emission").child(L"multiplier");
	if (emissionMult != nullptr)
	{
		const wchar_t* mval = (emissionMult.attribute(L"val") != nullptr) ? emissionMult.attribute(L"val").as_string() : emissionMult.text().as_string();
		newNode.append_child(L"magnitude").text() = mval;
	}

  // legacy internal crap 
  {
		pugi::xml_node refl = newNode.child(L"reflectivity");
		pugi::xml_node tran = newNode.child(L"transparency");

		if (refl.child(L"fresnel_ior").attribute(L"val") != nullptr) // copy from reflectivity.fresnel_ior tag to reflectivity.fresnel_IOR tag
			force_attrib(force_child(refl, L"fresnel_IOR"), L"val").set_value(refl.child(L"fresnel_ior").attribute(L"val").as_string());

		if (tran.child(L"ior").attribute(L"val") != nullptr) // copy from transparency.ior tag to transparency.IOR tag
			force_attrib(force_child(tran, L"IOR"), L"val").set_value(tran.child(L"ior").attribute(L"val").as_string());

		if (tran.child(L"thin_walled").attribute(L"val") != nullptr) // copy from transparency.thin_walled tag to transparency.thin_surface tag
			force_attrib(force_child(tran, L"thin_surface"), L"val").set_value(tran.child(L"thin_walled").attribute(L"val").as_string());

    auto displ = a_materialNode.child(L"displacement");
    if (displ.child(L"height_map") != nullptr)
    {
      auto displ2 = force_child(newNode, L"displacement");
      force_child(displ2, L"bump_amount").text() = displ.child(L"height_map").attribute(L"amount").as_string();

      force_child(displ2, L"bump_radius").text() = displ.child(L"height_map").attribute(L"smooth_lvl").as_float()*10.0f;
    }
    else if (displ.child(L"normal_map") != nullptr)
    {
      auto displ2 = force_child(newNode, L"displacement");
      force_child(displ2, L"invert_normalX").text() = displ.child(L"normal_map").child(L"invert").attribute(L"x").as_string();
      force_child(displ2, L"invert_normalY").text() = displ.child(L"normal_map").child(L"invert").attribute(L"y").as_string();

      force_child(displ2, L"bump_amount").text() = L"1";
      force_child(displ2, L"bump_radius").text() = L"1";
      force_child(displ2, L"bump_sigma").text()  = L"1";
      force_child(displ2, L"height").text()      = L"0";
    }   

    UpdateTextureSamplers(newNode);
    ValAttributeToText(newNode);

    pugi::xml_node relfGloss     = newNode.child(L"reflectivity").child(L"glossiness");
    pugi::xml_node relfGlossMult = newNode.child(L"reflectivity").child(L"glosiness_tex_mult");

		newNode.child(L"reflectivity").append_child(L"brdf_type").text() = a_materialNode.child(L"reflectivity").attribute(L"brdf_type").as_string();

    if (relfGloss != nullptr && relfGlossMult == nullptr)
    {
      pugi::xml_node gmul = newNode.child(L"reflectivity").append_child(L"glosiness_tex_mult");
      gmul.text() = relfGloss.text().as_string();
    }

    pugi::xml_node transp = newNode.child(L"transparency");

    if (transp != nullptr)
    {
      transp.append_child(L"exit_color").text() = L"1 1 1";
    }

    pugi::xml_node opacity = newNode.child(L"opacity");

    if(transp != nullptr && opacity == nullptr)
    {
      transp.append_child(L"skip_shadow").text() = L"0";
      //transp.append_child(L"shadow_matte").text() = L"0";
    }
    else if(opacity != nullptr)
    {
      if(transp == nullptr)
        transp = newNode.append_child(L"transparency");
      transp.append_child(L"skip_shadow").text().set(opacity.child(L"skip_shadow").attribute(L"val").as_string());
      //transp.append_child(L"shadow_matte").text().set(opacity.child(L"shadow_matte").attribute(L"val").as_string());
    }
		
		if (mtype == L"shadow_catcher")
		{
			if (transp == nullptr)
				transp = newNode.append_child(L"transparency");
		
			force_child(transp, L"skip_shadow").text()  = L"0";
			force_child(transp, L"shadow_matte").text() = L"1";
		}

    pugi::xml_node transpGloss = transp.child(L"glossiness");

    if (transpGloss != nullptr )
    {
      pugi::xml_node cosPower = transp.append_child(L"cos_power");
      cosPower.text() = GetCosPowerFromMaxShiness(transpGloss.text().as_float());
    }

    pugi::xml_node transpFogMult = transp.child(L"fog_multiplier");

    if (transpFogMult != nullptr)
    {
      pugi::xml_node typoFog = transp.append_child(L"fog_multiplyer");
      typoFog.text() = transpFogMult.text().as_string();
    }

  }

  // hydra material default value of cast_gi
  {
    pugi::xml_node emiss  = newNode.child(L"emission");
    pugi::xml_node castGI = emiss.child(L"cast_gi");

		if (castGI == nullptr)
			emiss.append_child(L"cast_GI").text() = L"1";
		else
			emiss.append_child(L"cast_GI").text() = castGI.attribute(L"val").as_string();

		pugi::xml_attribute eValue = a_materialNode.child(L"emission").child(L"multiplier").attribute(L"val");

		if (eValue != nullptr) // piece of shit
		{
			const HydraLiteMath::float3 ecolor = HydraXMLHelpers::ReadFloat3(a_materialNode.child(L"emission").child(L"color").attribute(L"val"));
			const float mult = eValue.as_float();

			const HydraLiteMath::float3 ecolor2 = ecolor*mult;

			std::wstringstream estream;
			estream << ecolor2.x << L" " << ecolor2.y << L" " << ecolor2.z;

			const std::wstring edata = estream.str();
			newNode.child(L"emission").child(L"color").text() = edata.c_str();

			//newNode.child(L"emission").append_child(L"magnitude").text() = eValue.as_string();
			newNode.child(L"emission").append_child(L"magnitude").text() = L"1";
		}
  }

  return true;
}

void clear_node(pugi::xml_node a_xmlNode);

bool RD_HydraLegacy::UpdateLight(int32_t a_lightIdId, pugi::xml_node a_lightNode)
{
  std::wstring ltype  = a_lightNode.attribute(L"type").as_string();
  std::wstring lshape = a_lightNode.attribute(L"shape").as_string();
  std::wstring ldistr = a_lightNode.attribute(L"distribution").as_string();


  std::wstringstream strout;
  strout << a_lightIdId;
  std::wstring strId = strout.str();

  pugi::xml_node nodeToCopy = m_lightsLib.find_child_by_attribute(L"id", strId.c_str());
  pugi::xml_node resultNode;

  if (nodeToCopy != nullptr)
  {
    resultNode = m_lightsLib.insert_copy_after(a_lightNode, nodeToCopy);
    m_lightsLib.remove_child(nodeToCopy);
  }
  else
    resultNode = m_lightsLib.append_copy(a_lightNode);

  // piece of legacy shit
  //
  pugi::xml_node originalColor = a_lightNode.child(L"intensity").child(L"color");

  if (originalColor.attribute(L"val") != nullptr)
  {
    auto colorNode = force_child(force_child(resultNode, L"intensity"), L"color");
    clear_node(colorNode);
    colorNode.text() = originalColor.attribute(L"val").as_string();
  }

	if (a_lightNode.child(L"intensity").child(L"multiplier").attribute(L"val") != nullptr)
	{
		force_child(force_child(resultNode, L"intensity"), L"multiplier").text() = a_lightNode.child(L"intensity").child(L"multiplier").attribute(L"val").as_string();
	}

  auto tex = originalColor.child(L"texture");

  if (tex != nullptr)
  {
    
    auto tex_id = tex.attribute(L"id").as_string();
    pugi::xml_node texNode = m_texturesLib.find_child_by_attribute(L"id", tex_id);

    auto tex_path = texNode.attribute(L"name").as_string();

    auto spheremapNode = force_child(resultNode, L"spheremap");
    //auto spheremapNode = resultNode.parent().parent().append_child(L"spheremap");
    force_child(spheremapNode, L"surface").text() = tex_path;

    auto samplerNode = spheremapNode.append_child(L"sampler");

    samplerNode.append_child(L"matrix").text().set(tex.attribute(L"matrix").as_string());
    samplerNode.append_child(L"addressing_mode_u").text().set(tex.attribute(L"addressing_mode_u").as_string());
    samplerNode.append_child(L"addressing_mode_v").text().set(tex.attribute(L"addressing_mode_v").as_string());
  }

  if(ldistr == L"ies")
  {
    auto iesPath = a_lightNode.child(L"ies").attribute(L"loc").as_string();
    auto matrix  = a_lightNode.child(L"ies").attribute(L"matrix").as_string();

    std::wstring iesPath2 = m_libPath + std::wstring(L"/") + iesPath;

    force_child(force_child(resultNode, L"distribution_ies"), L"ies_data").text()   = iesPath2.c_str();
    force_child(force_child(resultNode, L"distribution_ies"), L"matrix_rot").text() = matrix;
  }

  if (a_lightNode.child(L"sky_portal") != nullptr)
  {
    force_child(force_child(resultNode, L"general"), L"sky_portal").text() = a_lightNode.child(L"sky_portal").attribute(L"val").as_string();
    force_child(force_child(resultNode, L"general"), L"sky_portal_source").text() = a_lightNode.child(L"sky_portal").attribute(L"source").as_string();
  }

  if (a_lightNode.child(L"perez") != nullptr)
  {
    force_child(force_child(resultNode, L"perez"), L"sun_name").text() = a_lightNode.child(L"perez").attribute(L"sun_name").as_string();
    force_child(force_child(resultNode, L"perez"), L"turbidity").text() = a_lightNode.child(L"perez").attribute(L"turbidity").as_string();
  }

  if (a_lightNode.child(L"falloff_angle") != nullptr)
  {
    force_child(resultNode, L"falloff_angle").text() = a_lightNode.child(L"falloff_angle").attribute(L"val").as_string();
  }

  if (a_lightNode.child(L"falloff_angle2") != nullptr)
  {
    force_child(resultNode, L"falloff_angle2").text() = a_lightNode.child(L"falloff_angle2").attribute(L"val").as_string();
  }

  if (a_lightNode.attribute(L"shape") != nullptr)
  {

    if (ltype == L"area")
    {
      force_child(force_child(resultNode, L"general"), L"type").text()         = ltype.c_str();
      force_child(force_child(resultNode, L"general"), L"distribution").text() = ldistr.c_str();

      if (lshape == L"disk")
        resultNode.attribute(L"type").set_value(L"disk");
      else if(lshape == L"rect")
        resultNode.attribute(L"type").set_value(L"area");
      else if(lshape == L"sphere")
        resultNode.attribute(L"type").set_value(L"sphere");
    }
    else
    {
      if (ldistr == L"spot")
        resultNode.attribute(L"type").set_value(L"spot");
      else if (ltype == L"sky")
        resultNode.attribute(L"type").set_value(L"sky");
      else if(ldistr == L"omni")
        resultNode.attribute(L"type").set_value(L"point");
    }

  }

  if (ltype == L"directional")
  {
    resultNode.attribute(L"type").set_value(L"directional");
    resultNode.force_child(L"general").force_child(L"type").text() = L"directional";

    if (a_lightNode.child(L"size").attribute(L"inner_radius") != nullptr)
    {
      force_child(resultNode, L"inner_radius").text() = a_lightNode.child(L"size").attribute(L"inner_radius").as_string();
      force_child(resultNode, L"outer_radius").text() = a_lightNode.child(L"size").attribute(L"outer_radius").as_string();
    }

		if (a_lightNode.child(L"shadow_softness").attribute(L"val") != nullptr)
			force_child(resultNode, L"shadow_softness").text() = a_lightNode.child(L"shadow_softness").attribute(L"val").as_string();
  }

  // now we have to fix light size
  //
  // pugi::xml_node originalSizeNode = a_lightNode.child(L"size");
  if (lshape == L"disk")
  {
    float R = HydraXMLHelpers::ReadSphereOrDiskLightRadius(a_lightNode);
    force_child(force_child(resultNode, L"size"), L"radius").text() = R;
  }
  else if (lshape == L"rect")
  {
    HydraLiteMath::float2 a_size = HydraXMLHelpers::ReadRectLightSize(a_lightNode);
    force_child(force_child(resultNode, L"size"), L"Half-length").text() = a_size.x;
    force_child(force_child(resultNode, L"size"), L"Half-width").text()  = a_size.y;
  }
  else if (lshape == L"sphere")
  {
    float R = HydraXMLHelpers::ReadSphereOrDiskLightRadius(a_lightNode);
    force_child(force_child(resultNode, L"size"), L"radius").text() = R;
  }

	if (a_lightNode.attribute(L"visible") != nullptr)
	{
		if (a_lightNode.attribute(L"visible").as_int() == 0)
		{
			resultNode.append_child(L"hide_geometry").text().set(1);
			force_child(force_child(resultNode, L"general"), L"hideLightGeometry").text() = L"1";
		}
	}

  return true;
}

bool RD_HydraLegacy::UpdateMesh(int32_t a_meshId, pugi::xml_node a_meshNode, const HRMeshDriverInput& a_input, const HRBatchInfo* a_batchList, int32_t listSize)
{
  SimpleMesh& mesh = m_meshes[a_meshId];

  mesh.verticesPos.resize(0);
  mesh.verticesNorm.resize(0);
  mesh.verticesTexCoord.resize(0);
  mesh.triIndices.resize(0);
  mesh.matIndices.resize(0);

  mesh.verticesPos.insert(mesh.verticesPos.end(), a_input.pos4f, a_input.pos4f + a_input.vertNum * 4);
  mesh.verticesNorm.insert(mesh.verticesNorm.end(), a_input.norm4f, a_input.norm4f + a_input.vertNum * 4);
  mesh.verticesTexCoord.insert(mesh.verticesTexCoord.end(), a_input.texcoord2f, a_input.texcoord2f + a_input.vertNum * 2);

  mesh.triIndices.insert(mesh.triIndices.end(), a_input.indices, a_input.indices + a_input.triNum * 3);
  mesh.matIndices.insert(mesh.matIndices.end(), a_input.triMatIndices, a_input.triMatIndices + a_input.triNum);

  return true;
}


bool RD_HydraLegacy::UpdateMeshFromFile(int32_t a_meshId, pugi::xml_node a_meshNode, const wchar_t* a_fileName) 
{ 
  // HydraGeomData data;
  // data.read(a_fileName);
  // 
  // SimpleMesh& mesh = m_meshes[a_meshId];
  // 
  // mesh.verticesPos.resize(0);
  // mesh.verticesNorm.resize(0);
  // mesh.verticesTexCoord.resize(0);
  // mesh.triIndices.resize(0);
  // mesh.matIndices.resize(0);
  // 
  // const int vertNum = data.getVerticesNumber();
  // const int triNum  = data.getIndicesNumber()/3;
  // 
  // const float* pos4f      = data.getVertexPositionsFloat4Array();
  // const float* norm4f     = data.getVertexNormalsFloat4Array();
  // const float* texcoord2f = data.getVertexTexcoordFloat2Array();
  // const int* indices      = (int*)data.getTriangleVertexIndicesArray();
  // const int* mindices     = (int*)data.getTriangleMaterialIndicesArray();
  // 
  // mesh.verticesPos.AppendToTheEnd(mesh.verticesPos.end(), pos4f, pos4f + vertNum * 4);
  // mesh.verticesNorm.AppendToTheEnd(mesh.verticesNorm.end(), norm4f, norm4f + vertNum * 4);
  // mesh.verticesTexCoord.AppendToTheEnd(mesh.verticesTexCoord.end(), texcoord2f, texcoord2f + vertNum * 2);
  // 
  // mesh.triIndices.AppendToTheEnd(mesh.triIndices.end(), indices, indices + triNum * 3);
  // mesh.matIndices.AppendToTheEnd(mesh.matIndices.end(), mindices, mindices + triNum);

  return false; 
}

bool RD_HydraLegacy::UpdateCamera(pugi::xml_node a_camNode)
{
  pugi::xml_node nodeToCopy = m_camerasInst.find_child_by_attribute(L"id", L"0");

  if (nodeToCopy != nullptr)
  {
    m_camerasInst.insert_copy_after(a_camNode, nodeToCopy);
    m_camerasInst.remove_child(nodeToCopy);
  }
  else
    m_camerasInst.append_copy(a_camNode);

  return true;
}

bool RD_HydraLegacy::UpdateSettings(pugi::xml_node a_settingsNode)
{
  clear_node2(m_settings);

  for (pugi::xml_node node = a_settingsNode.first_child(); node != nullptr; node = node.next_sibling())
    m_settings.append_copy(node);

  m_settings.append_child(L"inColladaProfile").text() = L"empty.xml";
  m_settings.append_child(L"inSceneFile").text()      = L"empty.dae";

  std::wstring resolution = std::wstring(a_settingsNode.child(L"width").text().as_string()) + 
                            std::wstring(L"x") + 
                            std::wstring(a_settingsNode.child(L"height").text().as_string());

  m_settings.append_child(L"resolution").text()   = resolution.c_str();
  m_settings.append_child(L"rendermethod").text() = L"pathtracing";

  m_settings.append_child(L"frame_buffer_format").text()    = L"float4";   
  m_settings.append_child(L"nowindow").text()               = L"1";                   
  m_settings.append_child(L"ptRegenerate").text()           = L"1";               
  m_settings.append_child(L"renderer_type_internal").text() = L"OpenCL";
  m_settings.append_child(L"frommax").text()                = L"1";                    
  m_settings.append_child(L"rendered_layer").text()         = L"color";    

  m_settings.append_child(L"camType").text()   = L"UVN";
  //m_settings.append_child(L"camAspect").text() = L"90.0";

  if (m_settings.child(L"texInputGamma") == nullptr)
    m_settings.append_child(L"texInputGamma").text() = L"2.2";

  std::wstring causticMethod = a_settingsNode.child(L"method_caustic").text().as_string();
  if (causticMethod == L"mlt" || causticMethod == L"pathtracing")
    m_settings.append_child(L"ptCaustics").text() = L"1";
  else
    m_settings.append_child(L"ptCaustics").text() = L"0";

  const bool mltIsEnabled1 = (std::wstring(a_settingsNode.child(L"method_primary").text().as_string())   == L"mlt");
  const bool mltIsEnabled2 = (std::wstring(a_settingsNode.child(L"method_secondary").text().as_string()) == L"mlt");
  const bool mltIsEnabled3 = (std::wstring(a_settingsNode.child(L"method_tertiary").text().as_string())  == L"mlt");
  const bool mltIsEnabled4 = (std::wstring(a_settingsNode.child(L"method_caustic").text().as_string())   == L"mlt");

  if (mltIsEnabled1 || mltIsEnabled2 || mltIsEnabled3 || mltIsEnabled4)
    m_settings.append_child(L"enable_mlt").text() = L"1";
  else
    m_settings.append_child(L"enable_mlt").text() = L"0";

  m_presets.minrays = a_settingsNode.child(L"minRaysPerPixel").text().as_int();
  m_presets.maxrays = a_settingsNode.child(L"maxRaysPerPixel").text().as_int();
  m_presets.errHDR  = 0.01f*a_settingsNode.child(L"pt_error").text().as_float();

  m_width           = a_settingsNode.child(L"width").text().as_int();
  m_height          = a_settingsNode.child(L"height").text().as_int();

  return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////

void RD_HydraLegacy::ScanAndFixTextures(pugi::xml_node a_node)
{
  if (std::wstring(a_node.name()) == L"texture")
  {
    const wchar_t* idStr    = a_node.attribute(L"id").as_string();
    pugi::xml_node copyFrom = m_texturesLib.find_child_by_attribute(L"id", idStr);
    
    std::wstring absImagePath = GetAbsolutePath(copyFrom.attribute(L"name").as_string());

    //a_node.append_attribute(L"name") = copyFrom.attribute(L"name").as_string();
    a_node.text() = absImagePath.c_str();
  }
  else
  {
    for (pugi::xml_node child = a_node.first_child(); child != nullptr; child = child.next_sibling())
      ScanAndFixTextures(child);
  }
}

void RD_HydraLegacy::ScanAndFixIes(pugi::xml_node a_node)
{
  if (std::wstring(a_node.name()) == L"ies_data")
  {
    std::wstring absImagePath = GetAbsolutePath(a_node.text().as_string());

    //a_node.append_attribute(L"name") = copyFrom.attribute(L"name").as_string();
    a_node.text() = absImagePath.c_str();
  }
  else
  {
    for (pugi::xml_node child = a_node.first_child(); child != nullptr; child = child.next_sibling())
      ScanAndFixIes(child);
  }
}

void RD_HydraLegacy::InstanceAllMaterials(pugi::xml_node a_lib, pugi::xml_node a_inst)
{
  int numNodes = m_currAllocInfo.matNum;

  std::wstringstream strout;

  for (int i = 0; i < numNodes; i++)
  {
    std::wstringstream strout;
    strout << i;
    const std::wstring strId = strout.str();

    pugi::xml_node copyFrom = a_lib.find_child_by_attribute(L"id", strId.c_str());

    pugi::xml_node node; 
    
    if (copyFrom == nullptr)
    {
      node = a_inst.append_child(L"material");
      node.append_attribute(L"id").set_value(strId.c_str());
    }
    else
    {
      node = a_inst.append_copy(copyFrom);
    }

    node.append_attribute(L"maxid").set_value(i);
    ScanAndFixTextures(node);
  }
}

void RD_HydraLegacy::BeginScene()
{
  clear_node2(m_lightsInst);
  clear_node2(m_materialsInst);

  InstanceAllMaterials(m_materialsLib, m_materialsInst);

  m_progressVal   = 0.0f;
  m_avgBrightness = 1.0f;
  m_avgBCounter   = 0;

  m_stopThreadImmediately = false;
  haveUpdateFromMT        = false;
  hadFinalUpdate          = false;

  m_currLightIsntanceId = 0;

  m_wholeScene.clear();
  m_wholeScene.reserve(3000000,100000);
  
}

void RD_HydraLegacy::EndScene()
{

  if (!m_haveSkyLight) // insert black environment to prevent access violation errors of old hydra.exe 
  {
    pugi::xml_node skyLight = m_lightsInst.append_child(L"light");
    skyLight.append_attribute(L"name").set_value(L"MaxEnvironment");

    skyLight.append_child(L"position").text() = L"0 0 0";
    skyLight.append_child(L"direction").text() = L"0 -1 0";
    skyLight.append_child(L"general").append_child(L"type").text() = L"Sky";

    pugi::xml_node intensity = skyLight.append_child(L"intensity");
    intensity.append_child(L"color").text() = L"0 0 0";
    intensity.append_child(L"multiplier").text() = L"1";
  }

  // (1) save m_wholeScene to vsgf file
  //
  uint32_t verticesNum  = uint32_t(m_wholeScene.verticesPos.size() / 4);
  uint32_t indicesNum   = uint32_t(m_wholeScene.triIndices.size());
  uint32_t materialsNum = 0;
  uint32_t flags        = 0;

  if (verticesNum == 0 || indicesNum == 0)
  {
    m_msg = L"RD_HydraLegacy::EndScene: empty m_wholeScene vsgf file";
    return;
  }

  uint64_t fileSizeInBytes = uint64_t(m_wholeScene.verticesPos.size()*sizeof(float))  + 
                             uint64_t(m_wholeScene.verticesNorm.size()*sizeof(float)) + 
                             uint64_t(m_wholeScene.verticesTexCoord.size()*sizeof(float)) + 
                             uint64_t(m_wholeScene.triIndices.size()*sizeof(int)) + 
                             uint64_t(m_wholeScene.matIndices.size()*sizeof(int));

  std::ofstream fout("C:/[Hydra]/pluginFiles/scene.vsgf", std::ios::binary);

  fout.write((const char*)&fileSizeInBytes, sizeof(uint64_t));
  fout.write((const char*)&verticesNum, sizeof(uint32_t));
  fout.write((const char*)&indicesNum, sizeof(uint32_t));
  fout.write((const char*)&materialsNum, sizeof(uint32_t));
  fout.write((const char*)&flags, sizeof(uint32_t));

  fout.write((const char*)&m_wholeScene.verticesPos[0], sizeof(float) * 4 * verticesNum);
  fout.write((const char*)&m_wholeScene.verticesNorm[0], sizeof(float) * 4 * verticesNum);
  fout.write((const char*)&m_wholeScene.verticesTexCoord[0], sizeof(float) * 2 * verticesNum);

  fout.write((const char*)&m_wholeScene.triIndices[0], sizeof(uint32_t)*indicesNum);
  fout.write((const char*)&m_wholeScene.matIndices[0], sizeof(uint32_t)*(indicesNum / 3));

  fout.flush();
  fout.close();

  m_wholeScene.freeMem();

  // (2) save xml doc with materials, lights and camera
  //
  m_doc.save_file(L"C:/[Hydra]/pluginFiles/hydra_profile_generated.xml", L"  ");
  m_docSettings.save_file(L"C:/[Hydra]/pluginFiles/settings.xml", L"  ");

  // (3) run hydras
  //
  int width  = m_settings.child(L"width").text().as_int();
  int height = m_settings.child(L"height").text().as_int();
  
  ////////////////////////////////////////////////////////////////////////////////////////////////////

  std::vector<int> devList; 

  for (size_t i = 0; i < m_devList2.size(); i++)
  {
    if (m_devList2[i].isEnabled)
      devList.push_back(m_devList2[i].id);
  }
 
  if (m_devList2.size() == 0)
  {
    devList.resize(1);
    devList[0] = -1;
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////

  delete m_pConnection;
  m_pConnection = CreateHydraServerConnection(width, height, false, devList);

  if (m_pConnection != nullptr && m_pConnection->hasConnection())
  {
    std::wstringstream strOut;
    m_docSettings.print(strOut, L"  ");
    std::string settingsStr = std::string("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n") + ws2s(strOut.str());

    m_pConnection->updatePresets(settingsStr.c_str()); // #TODO: is thete anything wrong here ???

    RenderProcessRunParams params;

    params.compileShaders = false;
    params.debug          = LEGACY_DRIVER_DEBUG;
    params.enableMLT = std::wstring(m_settings.child(L"enable_mlt").text().as_string()) == L"1";
    params.normalPriorityCPU = false;
    params.showConsole = true;
    m_params = params;

    m_pConnection->runAllRenderProcesses(params, m_devList);

    ImageZ* pImageA = m_pConnection->imageA();
    if (pImageA != nullptr)
    {
      std::string firstMsg = "-node_t A -sid 0 -layer color -action start";
      if (!pImageA->SendMsg(firstMsg.c_str()))
        std::cerr << "ImageA csn't send message";
    }
  }

  // (4) free memory
  //
  m_wholeScene.freeMem();
  
  //for (size_t i = 0; i < m_meshes.size(); i++) //#TODO: Actucally we can not free this memory, need to think!
    //m_meshes[i].freeMem();

  m_firstUpdate = true;
  
  if (FB_UPDATE_MT)
  {
    m_threadFinished        = false;
    m_threadIsRun           = true;
    m_stopThreadImmediately = false;
    hadFinalUpdate          = false;

    //std::async(std::launch::async, &RD_HydraLegacy::UnionABImagesFunc, this);
    m_updateFbThread = std::move(std::thread(&RD_HydraLegacy::UnionABImagesFunc, this));
    std::this_thread::yield();
  }
}

void RD_HydraLegacy::Draw()
{
  // like glFinish();
}


static inline void mat4x4_mul_vec4(float r[4], const float M[16], const float v[4])
{
  float tmp[4];
  
  tmp[0] = M[4*0+0]*v[0] + M[4*0+1]*v[1] + M[4*0+2]*v[2] + M[4*0+3]*v[3];
  tmp[1] = M[4*1+0]*v[0] + M[4*1+1]*v[1] + M[4*1+2]*v[2] + M[4*1+3]*v[3];
  tmp[2] = M[4*2+0]*v[0] + M[4*2+1]*v[1] + M[4*2+2]*v[2] + M[4*2+3]*v[3];
  tmp[3] = M[4*3+0]*v[0] + M[4*3+1]*v[1] + M[4*3+2]*v[2] + M[4*3+3]*v[3];

  r[0] = tmp[0];
  r[1] = tmp[1];
  r[2] = tmp[2];
  r[3] = tmp[3];
}


void RD_HydraLegacy::InstanceMeshes(int32_t a_meshId, const float* a_matrices, int32_t a_instNum, const int* a_lightInstId)
{
  SimpleMesh& mesh = m_meshes[a_meshId];

  if (mesh.verticesPos.size() == 0)
  {
    m_msg = L"RD_HydraLegacy::InstanceMeshes: empty mesh";
    std::cout << "##InstanceMeshes: " << a_meshId << ", " << a_instNum << ", vnum = " << mesh.verticesPos.size() << ", inum = " << mesh.triIndices.size() << std::endl;
    return;
  }

  for (int32_t i = 0; i < a_instNum; i++)
  {
    const float* matrix = a_matrices + i * 16;
    SimpleMesh meshCopy = mesh;

    float testMatrix[16];
    for (int j = 0; j < 16; j++)
      testMatrix[j] = matrix[j];

    //std::cout << "##InstanceMeshes: " << a_meshId << ", " << a_instNum << ", vnum = " << meshCopy.verticesPos.size() << ", inum = " << meshCopy.triIndices.size() << std::endl;

    // apply matrices here !!!
    //
    for (size_t j = 0; j < meshCopy.verticesPos.size(); j += 4)
    {
      float* vpos  = &meshCopy.verticesPos[j];
      float* vnorm = &meshCopy.verticesNorm[j];

      vnorm[3] = 0.0f;

      mat4x4_mul_vec4(vpos,  matrix, vpos);
      mat4x4_mul_vec4(vnorm, matrix, vnorm);

      int a = 2;
    }

    // append mesh to scene
    //
    const size_t oldVertexNum = m_wholeScene.verticesPos.size() / 4;

    // append vertices
    //
    m_wholeScene.verticesPos.insert(m_wholeScene.verticesPos.end(), meshCopy.verticesPos.begin(), meshCopy.verticesPos.end());
    m_wholeScene.verticesNorm.insert(m_wholeScene.verticesNorm.end(), meshCopy.verticesNorm.begin(), meshCopy.verticesNorm.end());
    m_wholeScene.verticesTexCoord.insert(m_wholeScene.verticesTexCoord.end(), meshCopy.verticesTexCoord.begin(), meshCopy.verticesTexCoord.end());

    // now append triangle indices ...
    //
    for (size_t i = 0; i < meshCopy.triIndices.size(); i++)
      m_wholeScene.triIndices.push_back(int32_t(oldVertexNum) + meshCopy.triIndices[i]);

    // append per triangle material id
    //
    m_wholeScene.matIndices.insert(m_wholeScene.matIndices.end(), meshCopy.matIndices.begin(), meshCopy.matIndices.end());
  }
}


void RD_HydraLegacy::InstanceLights(int32_t a_light_id, const float* a_matrix, pugi::xml_node* a_custAttrArray, int32_t a_instNum, int32_t a_lightGroupId)
{
  std::wstringstream strout;
  strout << a_light_id;
  std::wstring strId = strout.str();

  pugi::xml_node copyFrom = m_lightsLib.find_child_by_attribute(L"id", strId.c_str());


  for (int32_t instId = 0; instId < a_instNum; instId++)
  {
    int32_t lightInstId = m_currLightIsntanceId + instId;
    const float* matrix = a_matrix + instId * 16;
 
    float translate[4];
    translate[0] = matrix[0 * 4 + 3];
    translate[1] = matrix[1 * 4 + 3];
    translate[2] = matrix[2 * 4 + 3];
    translate[3] = matrix[3 * 4 + 3];

    float matrixRot1[16];
    for (int i = 0; i < 16; i++)
      matrixRot1[i] = matrix[i];

    matrixRot1[0 * 4 + 3] = 0.0f;
    matrixRot1[1 * 4 + 3] = 0.0f;
    matrixRot1[2 * 4 + 3] = 0.0f;
    matrixRot1[3 * 4 + 3] = 1.0f;

    float direction[4] = {0,-1,0,0};
    mat4x4_mul_vec4(direction, matrixRot1, direction);

    float matrixRot[9];
    for (int i = 0; i < 3; i++)
    {
      matrixRot[i * 3 + 0] = matrix[i * 4 + 0];
      matrixRot[i * 3 + 1] = matrix[i * 4 + 1];
      matrixRot[i * 3 + 2] = matrix[i * 4 + 2];
    }


    strout.seekp(std::ios_base::beg); strout.str(L"");
    strout << translate[0] << L" " << translate[1] << L" " << translate[2];
    std::wstring translateStr = strout.str();

    strout.seekp(std::ios_base::beg); strout.str(L"");
    strout << direction[0] << L" " << direction[1] << L" " << direction[2];
    std::wstring dirStr = strout.str();

    strout.seekp(std::ios_base::beg); strout.str(L"");
    for (int i = 0; i < 9;i++)
      strout << matrixRot[i] << L" ";
    std::wstring matRotStr = strout.str();

    pugi::xml_node nodeToCopy = m_lightsInst.find_child_by_attribute(L"id", strId.c_str());
    pugi::xml_node lightInstNode;

    if (nodeToCopy != nullptr)
    {
      lightInstNode = m_lightsInst.insert_copy_after(copyFrom, nodeToCopy);
      m_lightsInst.remove_child(copyFrom);
    }
    else
      lightInstNode = m_lightsInst.append_copy(copyFrom);

    lightInstNode.attribute(L"id").set_value(lightInstId);

    force_child(lightInstNode, L"position").text()   = translateStr.c_str();
    force_child(lightInstNode, L"direction").text()  = dirStr.c_str();
    force_child(lightInstNode, L"matrix_rot").text() = matRotStr.c_str();

    std::wstring ltype  = copyFrom.attribute(L"type").as_string();
    std::wstring ltype2 = copyFrom.child(L"general").child(L"type").text().as_string();

    if (ltype == L"Sky" || ltype == L"sky" || ltype2 == L"Sky" || ltype2 == L"sky")
      m_haveSkyLight = true;

    ScanAndFixIes(lightInstNode);
  }  

  m_currLightIsntanceId += a_instNum;
}



HRRenderUpdateInfo RD_HydraLegacy::HaveUpdateNow()
{
  HRRenderUpdateInfo result;

	if (LEGACY_DRIVER_DEBUG)
	{
		result.finalUpdate  = true;
		result.haveUpdateFB = true;
		result.progress     = 100.0f;
		return result;
	}

  if (m_pConnection == nullptr)
    return result;

  ImageZ* pImageA = m_pConnection->imageA();
  ImageZ* pImageB = m_pConnection->imageB();

  const bool notNull   = (pImageA != nullptr) && (pImageB != nullptr);
  
  if (FB_UPDATE_MT)
    result.haveUpdateFB = haveUpdateFromMT;
  else
    result.haveUpdateFB = (notNull && !pImageB->IsEmpty()); 

  result.finalUpdate = (m_progressVal >= 99.9999f && result.haveUpdateFB) || (FB_UPDATE_MT && hadFinalUpdate);

  result.haveUpdateMSG = false;
  if (notNull)
  {
    std::string serverReply = pImageB->message();
    auto p                  = serverReply.find("[msg]");
    result.haveUpdateMSG    = (p != std::string::npos);

    if (result.haveUpdateMSG)
    {
      std::wstring lastServerReply = s2ws(serverReply);
      wcsncpy(m_lastServerReply, lastServerReply.c_str(), 256);
      result.msg = m_lastServerReply;
    }
  }

  result.progress = m_progressVal;
  return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<float> LoadGBuffer(const std::wstring& a_folderPath, int* pW, int* pH);

static inline int   __float_as_int(float x) { return *((int*)&x); }
static inline float __int_as_float(int x) { return *((float*)&x); }

static inline int   as_int(float x) { return __float_as_int(x); }
static inline float as_float(int x) { return __int_as_float(x); }

static inline void decodeNormal(unsigned int a_data, float a_norm[3])
{
  const float divInv = 1.0f / 32767.0f;

  short a_enc_x, a_enc_y;

  a_enc_x = (short)(a_data & 0x0000FFFF);
  a_enc_y = (short)((int)(a_data & 0xFFFF0000) >> 16);

  float sign = (a_enc_x & 0x0001) ? -1.0f : 1.0f;

  a_norm[0] = (short)(a_enc_x & 0xfffe)*divInv;
  a_norm[1] = (short)(a_enc_y & 0xfffe)*divInv;
  a_norm[2] = sign*sqrt(fmax(1.0f - a_norm[0] * a_norm[0] - a_norm[1] * a_norm[1], 0.0f));
}

typedef HRGBufferPixel GBuffer1;

static inline GBuffer1 unpackGBuffer1(const float a_input[4])
{
  GBuffer1 res;

  res.depth = a_input[0];
  res.matId = as_int(a_input[2]);
  decodeNormal(as_int(a_input[1]), res.norm);

  unsigned int rgba = as_int(a_input[3]);
  res.rgba[0] = (rgba & 0x000000FF)*(1.0f / 255.0f);
  res.rgba[1] = ((rgba & 0x0000FF00) >> 8)*(1.0f / 255.0f);
  res.rgba[2] = ((rgba & 0x00FF0000) >> 16)*(1.0f / 255.0f);
  res.rgba[3] = ((rgba & 0xFF000000) >> 24)*(1.0f / 255.0f);

  return res;
}


static inline float unpackAlpha(float a_input[4])
{
  const unsigned int rgba = as_int(a_input[3]);
  return ((rgba & 0xFF000000) >> 24)*(1.0f / 255.0f);
}

static float ReadFloatParam(const char* message, const char* paramName)
{
  std::istringstream iss(message);

  float res = 0.0f;

  do
  {
    std::string name, val;
    iss >> name >> val;

    if (name == paramName)
    {
      res = (float)(atof(val.c_str()));
      break;
    }

  } while (iss);

  return res;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void RD_HydraLegacy::GetFrameBufferColorFromABImages(int32_t w, int32_t h, float* a_out)
{
  ImageZ* pImageA = m_pConnection->imageA();
  ImageZ* pImageB = m_pConnection->imageB();

  const bool notNull = (pImageA != nullptr) && (pImageB != nullptr);

  if (notNull && !pImageB->IsEmpty() && pImageB->Lock(100))
  {
    std::string serverReply = pImageB->message();

    if (serverReply.find("[msg]") != std::string::npos) // just a text message without image, works in the beggining only
    {
      std::wstring msgUnicode = s2ws(serverReply.substr(5, serverReply.size() - 5));

      auto p1 = msgUnicode.find_first_of(L"[");
      auto p2 = msgUnicode.find_first_of(L"%]");

      if (p1 != std::wstring::npos && p2 != std::wstring::npos)
      {
        std::wstring number = msgUnicode.substr(p1 + 1, p2 - p1 - 1);
        m_progressVal = (float)(_wtof(number.c_str()));
      }

    }
    else
    {
      pImageA->UnionWith(pImageB);

      m_progressVal = 100.0f*pImageA->EstimateProgress(m_presets.errHDR, m_presets.minrays, m_presets.maxrays); // pt sample pixels and tiles

      if (m_fbDataHDR.size() != w*h * 4)
        m_fbDataHDR.resize(w*h * 4);

      ZImageUtils::ImageZBlockMemToRowPitch(pImageA->ColorPtr(0), &m_fbDataHDR[0], w, h);       // #TODO: implement iamge obtaining for MLT !!!
      memcpy(a_out, &m_fbDataHDR[0], w*h * sizeof(float) * 4);

      if (m_firstUpdate)
      {
        int w = 0;
        int h = 0;

        m_gbuffer = LoadGBuffer(L"C:/[Hydra]/temp/filter/gbuffer1.image4f", &w, &h);

        for (size_t i = 0; i < m_gbuffer.size(); i += 4)
        {
          float* data = &m_gbuffer[i];
          a_out[i + 3] = unpackAlpha(data);
        }

        m_firstUpdate = false;
      }
    }

    pImageB->Clear();
    pImageB->Unlock();

  }
  else if(m_fbDataHDR.size() > 0)
  {
    memcpy(a_out, &m_fbDataHDR[0], w*h*sizeof(float) * 4);
  }

}



static inline float contribFunc(double color[4])
{
  return (float)(fmax(0.33334f*(color[0] + color[1] + color[2]), 0.0));
}

void RD_HydraLegacy::CopyFromZMLT(int32_t w, int32_t h, float* a_data)
{
  struct float4 { float x, y, z, w; };

  ImageZ* pInput = m_pConnection->imageA();

  int zimageWidth  = pInput->Width();
  int zimageHeight = pInput->Height();

  if (zimageWidth != m_width || zimageHeight != m_height)
    return;

  float* colorP = pInput->ColorPtr(0);
  float* colorS = pInput->ColorPtr(1);

  if (colorP == nullptr || colorS == nullptr || a_data == nullptr)
    return;

  float4* colorP2 = (float4*)colorP;
  float4* colorS2 = (float4*)colorS;

  double avgB4[4] = { 0, 0, 0, 0 };

  for (int y = 0; y < m_height; y++)
  {
    for (int x = 0; x < m_width; x++)
    {
      int indexSrc = ZImageUtils::IndexZB(x, y, zimageWidth);
      float4 col = colorS2[indexSrc];

      if (isfinite(col.x)) avgB4[0] += col.x;
      if (isfinite(col.y)) avgB4[1] += col.y;
      if (isfinite(col.z)) avgB4[2] += col.z;
      if (isfinite(col.w)) avgB4[3] += col.w;
    }
  }

  avgB4[0] *= (1.0 / double(m_width*m_height));
  avgB4[1] *= (1.0 / double(m_width*m_height));
  avgB4[2] *= (1.0 / double(m_width*m_height));
  avgB4[3] *= (1.0 / double(m_width*m_height));

  const double avgbPlugin = contribFunc(avgB4);

  const float normConst = (float)(double(m_avgBrightness) / avgbPlugin);

  // median filter secondary light
  //
  if (m_tempImage.width() != m_width || m_tempImage.height() != m_height)
    m_tempImage.resize(m_width, m_height);

  float4* pImgData = (float4*)m_tempImage.data();

  for (int y = 0; y < m_height; y++)
  {
    for (int x = 0; x < m_width; x++)
    {
      int indexSrc = ZImageUtils::IndexZB(x, y, zimageWidth);
      int index1 = y*m_width + x;

      float4 indirect = colorS2[indexSrc];
      indirect.x *= normConst;
      indirect.y *= normConst;
      indirect.z *= normConst;
      indirect.w *= normConst;
      pImgData[index1] = indirect;
    }
  }

  if (m_enableMedianFilter)
    m_tempImage.medianFilterInPlace(m_medianthreshold, m_avgBrightness);

  // final pass, get image
  //
  for (int y = 0; y < m_height; y++)
  {
    for (int x = 0; x < m_width; x++)
    {
      int indexSrc = ZImageUtils::IndexZB(x, y, zimageWidth);

      float4 primary   = colorP2[indexSrc];
      float4 secondary = colorS2[indexSrc];

      int index1 = y*m_width + x;
      float4 secondary2 = pImgData[index1];

      a_data[index1 * 4 + 0] = primary.x + secondary2.x;
      a_data[index1 * 4 + 1] = primary.y + secondary2.y;
      a_data[index1 * 4 + 2] = primary.z + secondary2.z;
      //a_data[index1 * 4 + 3] = primary.w + secondary2.w;
    }
  }

}


void RD_HydraLegacy::GetFrameBufferColorFromABImagesMLT(int32_t w, int32_t h, float* a_out)
{
  ImageZ* pImageA = m_pConnection->imageA();
  ImageZ* pImageB = m_pConnection->imageB();

  const bool notNull = (pImageA != nullptr) && (pImageB != nullptr);

  if (notNull && !pImageB->IsEmpty() && pImageB->Lock(100))
  {
    std::string serverReply = pImageB->message();

    if (serverReply.find("[msg]") != std::string::npos) // just a text message without image, works in the beggining only
    {
      std::wstring msgUnicode = s2ws(serverReply.substr(5, serverReply.size() - 5));

      auto p1 = msgUnicode.find_first_of(L"[");
      auto p2 = msgUnicode.find_first_of(L"%]");

      if (p1 != std::wstring::npos && p2 != std::wstring::npos)
      {
        std::wstring number = msgUnicode.substr(p1 + 1, p2 - p1 - 1);
        m_progressVal = (float)(_wtof(number.c_str()));
      }

    }
    else
    {
      pImageA->UnionWith(pImageB);
     
      float avgBrightness = ReadFloatParam(pImageB->message(), "-avgb");
      float alpha = 1.0f / float(m_avgBCounter + 1);
      m_avgBrightness = avgBrightness*alpha + m_avgBrightness*(1.0f - alpha);
   
      m_progressVal = 100.0f*pImageA->EstimateProgress(0.0f, m_presets.maxrays, m_presets.maxrays); // mlt sample whole image
     
      CopyFromZMLT(w, h, a_out);

      if (m_firstUpdate)
      {
        int w = 0;
        int h = 0;

        m_gbuffer = LoadGBuffer(L"C:/[Hydra]/temp/filter/gbuffer1.image4f", &w, &h);

        for (size_t i = 0; i < m_gbuffer.size(); i += 4)
        {
          float* data = &m_gbuffer[i];
          a_out[i + 3] = unpackAlpha(data);
        }

        m_firstUpdate = false;
      }
    }

    pImageB->Clear();
    pImageB->Unlock();

  }
}


void RD_HydraLegacy::UnionABImagesFunc()
{
  ImageZ* pImageA = m_pConnection->imageA();
  ImageZ* pImageB = m_pConnection->imageB();

  while (true)
  {
    const bool notNull      = (pImageA != nullptr) && (pImageB != nullptr);
    const bool haveUpdateFB = (notNull && !pImageB->IsEmpty());
    
    bool finalUpdate = false;

    if (haveUpdateFB)
    { 
      ImageZ* pImageA = m_pConnection->imageA();
      ImageZ* pImageB = m_pConnection->imageB();

      const bool notNull = (pImageA != nullptr) && (pImageB != nullptr);

      if (notNull && !pImageB->IsEmpty() && pImageB->Lock(100))
      {
        std::string serverReply = pImageB->message();

        if (serverReply.find("[msg]") != std::string::npos) // just a text message without image, works in the beggining only
        {
          std::wstring msgUnicode = s2ws(serverReply.substr(5, serverReply.size() - 5));

          auto p1 = msgUnicode.find_first_of(L"[");
          auto p2 = msgUnicode.find_first_of(L"%]");

          if (p1 != std::wstring::npos && p2 != std::wstring::npos)
          {
            std::wstring number = msgUnicode.substr(p1 + 1, p2 - p1 - 1);
            m_progressVal = (float)(_wtof(number.c_str()));
          }

        }
        else
        {
          m_abMutex.lock();

          pImageA->UnionWith(pImageB);

          if (m_params.enableMLT)
          {
            float avgBrightness = ReadFloatParam(pImageB->message(), "-avgb");
            float alpha = 1.0f / float(m_avgBCounter + 1);
            m_avgBrightness = avgBrightness*alpha + m_avgBrightness*(1.0f - alpha);
          }

          m_progressVal = 100.0f*pImageA->EstimateProgress(m_presets.errHDR, m_presets.minrays, m_presets.maxrays); // pt sample pixels and tiles
          finalUpdate   = (m_progressVal >= 99.9999f) && haveUpdateFB;

          m_abMutex.unlock();

          haveUpdateFromMT = true;
          if (finalUpdate)
            hadFinalUpdate = true;
          // std::cout << "async framebuffer Update ... " << std::endl;
        }

        pImageB->Clear();
        pImageB->Unlock();

      }
    }

    if (finalUpdate || m_stopThreadImmediately) // info.finalUpdate
      break;

    std::this_thread::sleep_for(std::chrono::microseconds(250));
  }

  //if (m_stopThreadImmediately)
  //{
    std::stringstream sout2;
    sout2 << "-node_t A" << " -sid " << 0 << " -layer color " << " -action exitnow";
    pImageA->SendMsg(sout2.str().c_str());
  //}

  m_threadIsRun    = false;
  m_threadFinished = true;
}


void RD_HydraLegacy::GetFrameBufferHDR(int32_t w, int32_t h, float* a_out, const wchar_t* a_layerName)
{
  if (FB_UPDATE_MT)
  {
    m_abMutex.lock();

    ImageZ* pImageA = m_pConnection->imageA();

    if (m_params.enableMLT)
      CopyFromZMLT(w, h, a_out);
    else
      ZImageUtils::ImageZBlockMemToRowPitch(pImageA->ColorPtr(0), a_out, w, h);

    m_abMutex.unlock();

    haveUpdateFromMT = false;

    if (m_firstUpdate)
    {
      int w = 0;
      int h = 0;

      m_gbuffer = LoadGBuffer(L"C:/[Hydra]/temp/filter/gbuffer1.image4f", &w, &h);

      for (size_t i = 0; i < m_gbuffer.size(); i += 4)
      {
        float* data  = &m_gbuffer[i];
        a_out[i + 3] = unpackAlpha(data);
      }

      m_firstUpdate = false;
    }
  }
  else
  {
    std::wstring layer(a_layerName);
    if (layer == L"color" || layer == L"clr")
    {
      if (m_params.enableMLT)
        GetFrameBufferColorFromABImagesMLT(w, h, a_out);
      else
        GetFrameBufferColorFromABImages(w, h, a_out);
    }
  }

}

static inline float clamp(float u, float a, float b) { float r = fmax(a, u); return fmin(r, b); }
static inline int   clamp(int u, int a, int b) { int r = (a > u) ? a : u; return (r < b) ? r : b; }

static inline int RealColorToUint32(const float real_color[4])
{
  float  r = clamp(real_color[0]*255.0f, 0.0f, 255.0f);
  float  g = clamp(real_color[1]*255.0f, 0.0f, 255.0f);
  float  b = clamp(real_color[2]*255.0f, 0.0f, 255.0f);
  float  a = clamp(real_color[3]*255.0f, 0.0f, 255.0f);

  unsigned char red   = (unsigned char)r;
  unsigned char green = (unsigned char)g;
  unsigned char blue  = (unsigned char)b;
  unsigned char alpha = (unsigned char)a;

  return red | (green << 8) | (blue << 16) | (alpha << 24);
}

void RD_HydraLegacy::GetFrameBufferLDR(int32_t w, int32_t h, int32_t* a_out)
{
  m_fbDataHDR.resize(w*h * 4);
  GetFrameBufferHDR(w, h, &m_fbDataHDR[0], L"color");

  const float invGamma = 1.0f / 2.2f;

  #pragma omp parallel for
  for (int i = 0; i < w*h; i++)
  {
    const float r = m_fbDataHDR[i * 4 + 0];
    const float g = m_fbDataHDR[i * 4 + 1];
    const float b = m_fbDataHDR[i * 4 + 2];
    const float a = m_fbDataHDR[i * 4 + 3];

    const float color[4] = { powf(r, invGamma), 
                             powf(g, invGamma), 
                             powf(b, invGamma), 
                             a 
    };
    
    a_out[i] = RealColorToUint32(color);
  }


}

void RD_HydraLegacy::GetGBufferLine(int32_t a_lineNumber, HRGBufferPixel* a_lineData, int32_t a_startX, int32_t a_endX)
{
  if (a_endX > m_width)
    a_endX = m_width;

  const int32_t lineOffset = (a_lineNumber*m_width + a_startX);
  const int32_t lineSize   = (a_endX - a_startX);

  for (int32_t x = 0; x < lineSize; x++)
  {
    const float* data = &m_gbuffer[(lineOffset + x)*4];
    a_lineData[x] = unpackGBuffer1(data);
  }

}

void RD_HydraLegacy::ExecuteCommand(const wchar_t* a_cmd, wchar_t* a_out)
{
  if (m_pConnection == nullptr)
    return;

  ImageZ* pImageA = m_pConnection->imageA();

  if (pImageA != nullptr)
  {
    std::wstring inputW(a_cmd);
    std::string  inputA = ws2s(a_cmd);

    std::stringstream sout2;
    sout2 << "-node_t A" << " -sid 0 -layer color -action " << inputA.c_str();

    if (!pImageA->SendMsg(sout2.str().c_str()))
      m_msg = L"can't finish render due to connection->imageA() is busy or invalid ... ";
  }

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

IHRRenderDriver* CreateHydraLegacy_RenderDriver()
{
  return new RD_HydraLegacy;
}
