#include "RenderDriverHydraLegacyStuff.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <math.h>

#include "../hydra_api/HydraInternal.h" // for use hr_mkdir

#pragma warning(disable:4996) // for sprintf to be ok

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


ImageZ::ImageZ(const char* imageFileName, const char* mutexName, int a_width, int a_height, std::ostream* plog, int layersNum, bool a_attach) : zblocks(nullptr), m_imageFile(NULL), m_mutex(NULL), wasFinalOnce(false), m_pLog(plog)
{
  for (int i = 0; i < AUX_COLORS_MAX; i++)
    colors[i] = nullptr;

  //if (m_pLog != nullptr)
    //(*m_pLog) << "ImageZ::ImageZ()" << std::endl;

  ZImageUtils::initTables();

  if (a_attach)
    m_mutex = OpenMutexA(MUTEX_ALL_ACCESS, FALSE, mutexName);
  else
    m_mutex = CreateMutexA(NULL, FALSE, mutexName);

  if (m_mutex == NULL)
  {
    if (m_pLog != nullptr)
      (*m_pLog) << "[syscall failed]: ImageZ::ImageZ() -> OpenMutexA/CreateMutexA " << std::endl;
    return;
  }

  if (a_attach)
    m_imageFile = OpenFileMappingA(FILE_MAP_READ | FILE_MAP_WRITE, 0, imageFileName);
  else
  {
    size_t imSize = (a_width + 16)*(a_height + 16);
    m_bufferByteSize = (uint64_t)sizeof(ImageZInfo) + (uint64_t)imSize*(sizeof(float) * 4 * layersNum) + (uint64_t)imSize*sizeof(ZBlockT2) + (uint64_t)1024;
    DWORD imageSizeL = m_bufferByteSize & 0x00000000FFFFFFFF;
    DWORD imageSizeH = ((uint64_t)(m_bufferByteSize & 0xFFFFFFFF00000000)) >> 32;

    m_imageFile = CreateFileMappingA(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, imageSizeH, imageSizeL, imageFileName);
    if (m_imageFile == NULL)
    {
      if (m_pLog != nullptr)
        (*m_pLog) << "[syscall failed]: ImageZ::ImageZ() -> CreateFileMappingA " << std::endl;
      CloseHandle(m_mutex);
      return;
    }
  }

  char* data = (char*)MapViewOfFile(m_imageFile, FILE_MAP_WRITE | FILE_MAP_READ, 0, 0, 0);
  if (data == nullptr)
  {
    if (m_pLog != nullptr)
      (*m_pLog) << "[syscall failed]: ImageZ::ImageZ() -> MapViewOfFile " << std::endl;
    CloseHandle(m_imageFile);
    CloseHandle(m_mutex);
    return;
  }

  m_bufferData = data;
  m_pInfo = (ImageZInfo*)data;

  if (!a_attach)
  {
    m_pInfo->width = a_width;
    m_pInfo->height = a_height;
    m_pInfo->layersNum = layersNum;
  }

  InitFromBufferData();

  if (!a_attach)
    Clear();

}

void ImageZ::InitFromBufferData()
{
  int a_width = 0;
  int a_height = 0;

  char* data = (char*)m_bufferData;
  m_pInfo = (ImageZInfo*)data;  data += sizeof(ImageZInfo);

  a_width = m_pInfo->width;
  a_height = m_pInfo->height;

  m_zblockCount = (m_pInfo->width*m_pInfo->height) / ZBLOCKSIZE;

  size_t offset1 = (a_width + 16)*(a_height + 16)*sizeof(float) * 4;
  size_t offset3 = m_zblockCount*sizeof(ZBlockT2);

  for (int i = 0; i < AUX_COLORS_MAX; i++)
    colors[i] = nullptr;

  for (int i = 0; i < m_pInfo->layersNum; i++)
  {
    colors[i] = (float*)data;
    data += offset1;
  }

  zblocks = (ZBlockT2*)data;
  data += offset3;

  commands = data;

  size_t imSize = (m_pInfo->width + 16)*(m_pInfo->height + 16);
  m_bufferByteSize = (uint64_t)sizeof(ImageZInfo) + (uint64_t)imSize*(sizeof(float) * 4 * m_pInfo->layersNum) + (uint64_t)imSize*sizeof(ZBlockT2) + (uint64_t)1024;

  memset(lastMsg, 0, 1024);
  lastMsgId = 0;
  lastMsgRcv = -1;
}


ImageZ::~ImageZ()
{
  UnmapViewOfFile(m_bufferData); m_bufferData = nullptr;
  CloseHandle(m_imageFile);
  CloseHandle(m_mutex);
}

void ImageZ::Clear()
{
  int size = m_pInfo->width*m_pInfo->height;

  for (int i = 0; i < m_pInfo->layersNum; i++)
    memset(colors[i], 0, size*sizeof(float) * 4);

  memset(commands, 0, 1024);

  for (int i = 0; i < m_zblockCount; i++)
    zblocks[i] = ZBlockT2();

  m_pInfo->flags = ZIMAGE_IS_EMPTY;

  wasFinalOnce = false;
}

bool ImageZ::IsEmpty() const
{
  return (m_pInfo->flags & ZIMAGE_IS_EMPTY);
}

namespace ZImageUtils
{
  struct float2
  {
    float2() : x(0.0f), y(0.0f) {}
    float2(float a_x, float a_y) : x(a_x), y(a_y) {}

    float x, y;
  };

  struct float3
  {
    float3() : x(0.0f), y(0.0f), z(0.0f) {}
    float3(float a_x, float a_y, float a_z) : x(a_x), y(a_y), z(a_z) {}

    float x, y, z;
  };

  struct float4
  {
    float x, y, z, w;
  };

  static inline float MonteCarloRelErr(float3 avgColor, float sqrColor, int nSamples)
  {
    const float maxColor   = fmaxf(avgColor.x, fmaxf(avgColor.y, avgColor.z));
    const float fnSamples  = ((float)(nSamples));
    const float nSampleInv = 1.0f / fnSamples;

    const float variance = fabs(sqrColor*nSampleInv - (maxColor*maxColor*nSampleInv*nSampleInv));
    const float stdError = sqrt(variance);

    return stdError / (fmax(maxColor, 0.00001f));
  }

  static uint16_t MortonTable256Host[] =
  {
    0x0000, 0x0001, 0x0004, 0x0005, 0x0010, 0x0011, 0x0014, 0x0015,
    0x0040, 0x0041, 0x0044, 0x0045, 0x0050, 0x0051, 0x0054, 0x0055,
    0x0100, 0x0101, 0x0104, 0x0105, 0x0110, 0x0111, 0x0114, 0x0115,
    0x0140, 0x0141, 0x0144, 0x0145, 0x0150, 0x0151, 0x0154, 0x0155,
    0x0400, 0x0401, 0x0404, 0x0405, 0x0410, 0x0411, 0x0414, 0x0415,
    0x0440, 0x0441, 0x0444, 0x0445, 0x0450, 0x0451, 0x0454, 0x0455,
    0x0500, 0x0501, 0x0504, 0x0505, 0x0510, 0x0511, 0x0514, 0x0515,
    0x0540, 0x0541, 0x0544, 0x0545, 0x0550, 0x0551, 0x0554, 0x0555,
    0x1000, 0x1001, 0x1004, 0x1005, 0x1010, 0x1011, 0x1014, 0x1015,
    0x1040, 0x1041, 0x1044, 0x1045, 0x1050, 0x1051, 0x1054, 0x1055,
    0x1100, 0x1101, 0x1104, 0x1105, 0x1110, 0x1111, 0x1114, 0x1115,
    0x1140, 0x1141, 0x1144, 0x1145, 0x1150, 0x1151, 0x1154, 0x1155,
    0x1400, 0x1401, 0x1404, 0x1405, 0x1410, 0x1411, 0x1414, 0x1415,
    0x1440, 0x1441, 0x1444, 0x1445, 0x1450, 0x1451, 0x1454, 0x1455,
    0x1500, 0x1501, 0x1504, 0x1505, 0x1510, 0x1511, 0x1514, 0x1515,
    0x1540, 0x1541, 0x1544, 0x1545, 0x1550, 0x1551, 0x1554, 0x1555,
    0x4000, 0x4001, 0x4004, 0x4005, 0x4010, 0x4011, 0x4014, 0x4015,
    0x4040, 0x4041, 0x4044, 0x4045, 0x4050, 0x4051, 0x4054, 0x4055,
    0x4100, 0x4101, 0x4104, 0x4105, 0x4110, 0x4111, 0x4114, 0x4115,
    0x4140, 0x4141, 0x4144, 0x4145, 0x4150, 0x4151, 0x4154, 0x4155,
    0x4400, 0x4401, 0x4404, 0x4405, 0x4410, 0x4411, 0x4414, 0x4415,
    0x4440, 0x4441, 0x4444, 0x4445, 0x4450, 0x4451, 0x4454, 0x4455,
    0x4500, 0x4501, 0x4504, 0x4505, 0x4510, 0x4511, 0x4514, 0x4515,
    0x4540, 0x4541, 0x4544, 0x4545, 0x4550, 0x4551, 0x4554, 0x4555,
    0x5000, 0x5001, 0x5004, 0x5005, 0x5010, 0x5011, 0x5014, 0x5015,
    0x5040, 0x5041, 0x5044, 0x5045, 0x5050, 0x5051, 0x5054, 0x5055,
    0x5100, 0x5101, 0x5104, 0x5105, 0x5110, 0x5111, 0x5114, 0x5115,
    0x5140, 0x5141, 0x5144, 0x5145, 0x5150, 0x5151, 0x5154, 0x5155,
    0x5400, 0x5401, 0x5404, 0x5405, 0x5410, 0x5411, 0x5414, 0x5415,
    0x5440, 0x5441, 0x5444, 0x5445, 0x5450, 0x5451, 0x5454, 0x5455,
    0x5500, 0x5501, 0x5504, 0x5505, 0x5510, 0x5511, 0x5514, 0x5515,
    0x5540, 0x5541, 0x5544, 0x5545, 0x5550, 0x5551, 0x5554, 0x5555
  };

  static inline uint32_t ZIndexHost(uint16_t x, uint16_t y)
  {
    return	MortonTable256Host[y >> 8] << 17 |
      MortonTable256Host[x >> 8] << 16 |
      MortonTable256Host[y & 0xFF] << 1 |
      MortonTable256Host[x & 0xFF];
  }

  uint32_t g_ztable2D16[16][16];

  void initTables()
  {
    for (int y = 0; y < 16; y++)
      for (int x = 0; x < 16; x++)
        g_ztable2D16[x][y] = ZIndexHost(x, y);
  }

  /*const int Z_ORDER_BLOCK_SIZE = 16;

  uint32_t IndexZB(int x, int y, int pitch)
  {
  uint32_t zOrderX = x % Z_ORDER_BLOCK_SIZE;
  uint32_t zOrderY = y % Z_ORDER_BLOCK_SIZE;

  uint32_t zIndex = ZIndexHost(zOrderX, zOrderY);

  uint32_t wBlocks = pitch / Z_ORDER_BLOCK_SIZE;
  uint32_t blockX  = x / Z_ORDER_BLOCK_SIZE;
  uint32_t blockY  = y / Z_ORDER_BLOCK_SIZE;

  return (blockX + (blockY)*(wBlocks))*(Z_ORDER_BLOCK_SIZE*Z_ORDER_BLOCK_SIZE) + zIndex;
  }*/


  inline uint32_t IndexZB(int x, int y, int pitch)
  {
    return (((x >> 4) + (y >> 4)*(pitch >> 4)) << 8) + g_ztable2D16[(x & 0x0000000F)][(y & 0x0000000F)];
  }

  inline uint32_t Index2D(uint32_t x, uint32_t y, int pitch) { return y*pitch + x; }


  void ImageZBlockMemToRowPitch(const float* inData, float* outData, int w, int h)
  {
    for (int y = 0; y<h; y++)
    {
      for (int x = 0; x<w; x++)
      {
        auto indexSrc = IndexZB(x, y, w);
        auto indexDst = Index2D(x, y, w);
        outData[indexDst*4+0] = inData[indexSrc*4+0];
        outData[indexDst*4+1] = inData[indexSrc*4+1];
        outData[indexDst*4+2] = inData[indexSrc*4+2];
        //outData[indexDst*4+3] = inData[indexSrc*4+3];
      }
    }

  }


  float BlockError(int offset, float* a_color, int nSamples, float a_pathTraceError)
  {
    float error = 0.0f;

    size_t start = offset;
    size_t end = offset + ImageZ::ZBLOCKSIZE;

    const float ptErrorInv = 1.0f / fmaxf(a_pathTraceError, 1e-10f);

    for (size_t x = start; x < end; x++)
    {
      float3 color;

      color.x = a_color[x * 4 + 0];
      color.y = a_color[x * 4 + 1];
      color.z = a_color[x * 4 + 2];

      float colorSquare = a_color[x * 4 + 3];
      float pixelError = MonteCarloRelErr(color, colorSquare, nSamples);

      if (pixelError >= a_pathTraceError)
      {
        float fuckedUp = (pixelError*ptErrorInv);  // how many times we are fucked up with error ?
        error += fuckedUp*fuckedUp;
      }
    }

    return error;
  }

  static bool BlockFinished(ZBlockT2 block, int a_minRaysPerPixel, int a_maxRaysPerPixel) // for use on the cpu side ... for current
  {
    if (block.index2 == 0xFFFFFFFF)
      return true;

    int samplesPerPixel = block.counter;
    float acceptedBadPixels = 8.0f; // sqrtf((float)(256));
    bool summErrorOk = (block.diff <= acceptedBadPixels);

    return ((summErrorOk && samplesPerPixel >= a_minRaysPerPixel) || (samplesPerPixel >= a_maxRaysPerPixel)) || (block.index2 == 0xFFFFFFFF);
  }

};


bool haveSameIds(ImageZ* pA, const ImageZ* pB)
{
  const char* msg1 = pA->message();
  const char* msg2 = pB->message();

  std::string sid1 = "0";
  std::string sid2 = "0";

  std::string mid1 = "0";
  std::string mid2 = "0";

  std::string layer1 = "color";
  std::string layer2 = "color";

  std::stringstream inCmd1(msg1);
  std::stringstream inCmd2(msg2);

  char name[64];
  char value[64];

  while (inCmd1.good())
  {
    inCmd1 >> name >> value;

    if (std::string(name) == "-layer")
      layer1 = value;

    if (std::string(name) == "-sid")
      sid1 = value;

    if (std::string(name) == "-mid")
      mid1 = value;
  }

  int finalImageB = 0;

  while (inCmd2.good())
  {
    inCmd2 >> name >> value;

    if (std::string(name) == "-layer")
      layer2 = value;

    if (std::string(name) == "-sid")
      sid2 = value;

    if (std::string(name) == "-mid")
      mid2 = value;

    if (std::string(name) == "-final")
      finalImageB = atoi(value);
  }

  pA->wasFinalOnce = pA->wasFinalOnce || (finalImageB == 1);

  return (sid1 == sid2) && (layer1 == layer2) && (mid1 == mid2);
}

void ImageZ::UnionWith(const ImageZ* a_pOtherImage)
{
  if (!haveSameIds(this, a_pOtherImage)) // if server responce is valid
  {
    const char* msg1 = this->message();
    const char* msg2 = a_pOtherImage->message();

    (*m_pLog) << "[ABImages]: Render responce is REJECTED! " << std::endl;
    (*m_pLog) << "  msg(A) = " << msg1 << std::endl;
    (*m_pLog) << "  msg(B) = " << msg2 << std::endl;

    return;
  }

  // add spectial case for emmpy ImageZ to reply general purpose messages
  //

  // union image. Main color or primary for MLT.
  //
  for (size_t z = 0; z < m_zblockCount; z++)
  {
    auto block2 = a_pOtherImage->zblocks[z];  int z2 = block2.index; // usually z2 == z, but anything is possible in future
    auto block1 = zblocks[z2];

    size_t start = block2.index*ZBLOCKSIZE;
    size_t end   = block2.index*ZBLOCKSIZE + ZBLOCKSIZE;

    const bool countersAreValid = ((block1.counter >= 0) && (block2.counter > 0));

    if (block1.index2 != 0xFFFFFFFF && countersAreValid) // && block2.index2 != 0xFFFFFFFF
    {
      float w1 = float(block1.counter) / float(block1.counter + block2.counter);
      float w2 = float(block2.counter) / float(block1.counter + block2.counter);

      float* color = this->ColorPtr();
      const float* otherColor = a_pOtherImage->ColorPtr();

      for (size_t x = start; x < end; x++) // can opt with sse, but need to be sure poiners are aligned !!!
      {
        float otherColorX = otherColor[x * 4 + 0];
        float otherColorY = otherColor[x * 4 + 1];
        float otherColorZ = otherColor[x * 4 + 2];
        float otherColorW = otherColor[x * 4 + 3];

        float oldColorX = color[x * 4 + 0];
        float oldColorY = color[x * 4 + 1];
        float oldColorZ = color[x * 4 + 2];
        float oldColorW = color[x * 4 + 3];

        color[x * 4 + 0] = oldColorX * w1 + otherColorX * w2;
        color[x * 4 + 1] = oldColorY * w1 + otherColorY * w2;
        color[x * 4 + 2] = oldColorZ * w1 + otherColorZ * w2;
        color[x * 4 + 3] = oldColorW * w1 + otherColorW * w2;
      }

      zblocks[z2].index   = z2;
      zblocks[z2].counter = block1.counter + block2.counter;
      zblocks[z2].index2  = block1.index2; // | block2.index2;    // resulting block finished if one of them is finished --> old way, does not applied now
    }
  }

  // union secondary for MLT
  //
  if (this->ColorPtr(1) != nullptr)
  {
    for (size_t z = 0; z < m_zblockCount; z++)
    {
      auto block2 = a_pOtherImage->zblocks[z];  int z2 = block2.index; // usually z2 == z, but anything is possible in future
      auto block1 = zblocks[z2];

      size_t start = block2.index*ZBLOCKSIZE;
      size_t end = block2.index*ZBLOCKSIZE + ZBLOCKSIZE;

      float* color = this->ColorPtr(1);
      const float* otherColor = a_pOtherImage->ColorPtr(1);

      for (size_t x = start; x < end; x++) // can opt with sse, but need to be sure poiners are aligned !!!
      {
        float otherColorX = otherColor[x * 4 + 0];
        float otherColorY = otherColor[x * 4 + 1];
        float otherColorZ = otherColor[x * 4 + 2];
        float otherColorW = otherColor[x * 4 + 3];

        float oldColorX = color[x * 4 + 0];
        float oldColorY = color[x * 4 + 1];
        float oldColorZ = color[x * 4 + 2];
        float oldColorW = color[x * 4 + 3];

        color[x * 4 + 0] = oldColorX + otherColorX;
        color[x * 4 + 1] = oldColorY + otherColorY;
        color[x * 4 + 2] = oldColorZ + otherColorZ;
        color[x * 4 + 3] = oldColorW + otherColorW;
      }

    }
  }

}

float ImageZ::EstimateProgress(float relError, int minRaysPerPixel, int maxRaysPerPixel)
{
  int BLOCK_NUMBER = (m_pInfo->width*m_pInfo->height) / ZBLOCKSIZE;
  float totalSppmf = float(BLOCK_NUMBER)*float(maxRaysPerPixel);
  int totalSPPDone = 0;

  for (size_t z = 0; z < m_zblockCount; z++)
  {
    auto& block = zblocks[z];
    block.diff = ZImageUtils::BlockError(block.index*ZBLOCKSIZE, ColorPtr(), block.counter, relError);

    if (ZImageUtils::BlockFinished(block, minRaysPerPixel, maxRaysPerPixel))
    {
      totalSPPDone += maxRaysPerPixel;
      block.index2 = 0xFFFFFFFF; // mark as finished
    }
    else
      totalSPPDone += block.counter;
  }

  return (float(totalSPPDone) / totalSppmf);
}

bool ImageZ::Lock(int a_miliseconds)
{
  const DWORD res = WaitForSingleObject(m_mutex, a_miliseconds);

  if (res == WAIT_FAILED)
  {
    if (m_pLog != nullptr)
      (*m_pLog) << "[syscall failed]: ImageZ::Lock() -> WaitForSingleObject() " << std::endl;
  }

  if (res == WAIT_TIMEOUT || res == WAIT_FAILED)
    return false;
  else
    return true;
}

void ImageZ::Unlock()
{
  ReleaseMutex(m_mutex);
}

void ImageZ::SaveToFile(const wchar_t* fname)
{
  std::ofstream fout(fname, std::ios::binary);

  if (!fout.is_open())
    return;

  fout.write((const char*)&m_bufferByteSize, sizeof(uint64_t));
  fout.write((const char*)m_bufferData, m_bufferByteSize);
  fout.flush();
  fout.close();
}


bool ImageZ::LoadFromFile(const wchar_t* fname)
{
  std::ifstream fin(fname, std::ios::binary);

  if (!fin.is_open())
    return false;

  uint64_t buffSize = 0;
  fin.read((char*)&buffSize, sizeof(uint64_t));

  if (buffSize != m_bufferByteSize)
  {
    std::cerr << "ImageZ::LoadFromFile: failed to load due to different size of image in file and buffer" << std::endl;
    return false;
  }

  fin.read((char*)m_bufferData, m_bufferByteSize);
  fin.close();

  InitFromBufferData();

  return true;
}

struct Pixel
{
  unsigned char r, g, b;
};

static void WriteBMP(const wchar_t* fname, Pixel* a_pixelData, int width, int height)
{
  BITMAPFILEHEADER bmfh;
  BITMAPINFOHEADER info;

  memset(&bmfh, 0, sizeof(BITMAPFILEHEADER));
  memset(&info, 0, sizeof(BITMAPINFOHEADER));

  int paddedsize = (width*height)*sizeof(Pixel);

  bmfh.bfType      = 0x4d42;       // 0x4d42 = 'BM'
  bmfh.bfReserved1 = 0;
  bmfh.bfReserved2 = 0;
  bmfh.bfSize      = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + paddedsize;
  bmfh.bfOffBits   = 0x36;

  info.biSize = sizeof(BITMAPINFOHEADER);
  info.biWidth = width;
  info.biHeight = height;
  info.biPlanes = 1;
  info.biBitCount = 24;
  info.biCompression = BI_RGB;
  info.biSizeImage = 0;
  info.biXPelsPerMeter = 0x0ec4;
  info.biYPelsPerMeter = 0x0ec4;
  info.biClrUsed = 0;
  info.biClrImportant = 0;

  std::ofstream out(fname, std::ios::out | std::ios::binary);
  out.write((const char*)&bmfh, sizeof(BITMAPFILEHEADER));
  out.write((const char*)&info, sizeof(BITMAPINFOHEADER));
  out.write((const char*)a_pixelData, paddedsize);
  out.flush();
  out.close();
}


void ImageZ::SaveToBMP(const wchar_t* fname, float a_gamma)
{
  int width  = m_pInfo->width;
  int height = m_pInfo->height;

  float gammaInv = 1.0f / a_gamma;

  ZImageUtils::float4* inColor = (ZImageUtils::float4*)ColorPtr();
  Pixel* pxdata = new Pixel[width*height];

  for (int y = 0; y<height; y++)
  {
    for (int x = 0; x<width; x++)
    {
      int indexSrc = ZImageUtils::IndexZB(x, y, width);
      int indexDst = ZImageUtils::Index2D(x, y, width);

      ZImageUtils::float4 colorf = inColor[indexSrc];

      colorf.x = fmin(powf(colorf.x, gammaInv), 1.0f);
      colorf.y = fmin(powf(colorf.y, gammaInv), 1.0f);
      colorf.z = fmin(powf(colorf.z, gammaInv), 1.0f);

      Pixel colorub;
      colorub.r = (unsigned char)(255.0f*colorf.z);
      colorub.g = (unsigned char)(255.0f*colorf.y);
      colorub.b = (unsigned char)(255.0f*colorf.x);

      pxdata[indexDst] = colorub;
    }
  }

  WriteBMP(fname, pxdata, width, height);

  delete[] pxdata;
}

void ImageZ::SaveToImage4f(const wchar_t* a_path)
{
  int width  = m_pInfo->width;
  int height = m_pInfo->height;

  ZImageUtils::float4* inColor = (ZImageUtils::float4*)ColorPtr();
  std::vector<ZImageUtils::float4> pxdata(width*height);

  for (int y = 0; y<height; y++)
  {
    for (int x = 0; x<width; x++)
    {
      int indexSrc = ZImageUtils::IndexZB(x, y, width);
      int indexDst = ZImageUtils::Index2D(x, y, width);

      pxdata[indexDst] = inColor[indexSrc];
    }
  }

  int wh[2] = { width, height };

  std::ofstream fout(a_path, std::ios::binary);
  fout.write((const char*)wh, sizeof(int) * 2);
  fout.write((const char*)&pxdata[0], width*height*sizeof(ZImageUtils::float4));
  fout.close();
}


bool ImageZ::SendMsg(const char* a_msg, bool inc_mid)
{
  if (this->Lock(250))
  {
    char temp[64];
    memset(temp, 0, 64);
    memset(message(), 0, 1024);

    if (inc_mid)
    {
      sprintf(temp, " -mid %d", lastMsgId);
      strncpy(message(), a_msg, 1024);
      strncat(message(), temp, 1024);
      lastMsgId++;
    }
    else
      strcpy(message(), a_msg);

    this->Unlock();
    return true;
  }
  else
    return false;
}

const char* ImageZ::RecieveMsg()
{
  char name[64];
  char value[64];

  if (this->Lock(0))
  {
    int thisMessageId = -1;

    std::stringstream msgInput(message());

    while (msgInput.good())
    {
      msgInput >> name >> value;
      if (std::string(name) == "-mid")
      {
        thisMessageId = atoi(value);
        break;
      }
    }

    if (lastMsgRcv < thisMessageId)
    {
      memcpy(lastMsg, message(), 1024);
      lastMsgRcv = thisMessageId;
      this->Unlock();
      return lastMsg;
    }
    else
    {
      this->Unlock();
      return nullptr;
    }

  }
  else
    return nullptr;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool g_materialProcessStart = false;
PROCESS_INFORMATION g_materialProcessInfo;

struct PluginShmemPipe : public IHydraNetPluginAPI
{
  PluginShmemPipe(const char* imageFileName, const char* messageFileName, const char* guiFile, int width, int height,
                  const char* connectionType = "main",
                  const std::vector<int>& a_devIdList = std::vector<int>(),
                  std::ostream* m_pLog = nullptr);

  virtual ~PluginShmemPipe();

  void updatePresets(const char* a_presetsXML);
  bool hasConnection() const;
  bool isStatic() { return m_staticConnection; }

  void runAllRenderProcesses(RenderProcessRunParams a_params, const std::vector<HydaRenderDevice>& a_devList);
  void stopAllRenderProcesses();

  ImageZ* imageA() { return pImageA; }
  ImageZ* imageB() { return pImageB; }

  HANDLE getMtlRenderHProcess() const { return g_materialProcessInfo.hProcess; }

protected:

  ImageZ* pImageA;
  ImageZ* pImageB;

  int imageWidth();
  int imageHeight();

  void CreateConnectionMainType(const char* imageFileName, const char* messageFileName, const char* guiFile, int width, int height);
  void CreateConnectionMaterialType(const char* imageFileName, const char* messageFileName, const char* guiFile, int width, int height);

  HANDLE m_hFile, m_hImageFile, m_hGuiFile;
  HANDLE m_bvhMutex;
  char* m_shmem;
  char* m_imageShmem;
  char* m_guiShmem;

  STARTUPINFOA m_hydraStartupInfo;
  PROCESS_INFORMATION m_hydraProcessInfo;
  BOOL m_hydraServerStarted;
  bool m_staticConnection;

  // when multi-device mode is used
  bool m_multiDevMode;
  std::vector<PROCESS_INFORMATION> m_mdProcessList;
  std::vector<int>                 m_mdDeviceList;
  // \\

  unsigned int m_lastImageType;

  enum { MESSAGE_BUFF_SIZE = 1024 };

  struct SharedBufferDataInfo* bufferInfo() { return (struct SharedBufferDataInfo*)m_shmem; }
  char* bufferData() { return m_shmem + sizeof(struct SharedBufferDataInfo); }

  struct SharedBufferDataInfo* imageBufferInfo() { return (struct SharedBufferDataInfo*)m_imageShmem; }
  char* imageBufferDara() { return m_imageShmem + sizeof(struct SharedBufferDataInfo); }

  // params that we have to remember after connection created
  //
  std::string m_connectionType;

  std::string m_guiFileName;
  std::string m_messageFileName;
  std::string m_imageFileName;

  int m_width;
  int m_height;

  std::ostream* m_pLog;
};

static IHydraNetPluginAPI* g_pMaterialRenderConnect = nullptr;
static std::ofstream g_logMain;
static std::ofstream g_logMtl;

IHydraNetPluginAPI* CreateHydraServerConnection(int renderWidth, int renderHeight, bool inMatEditor, const std::vector<int>& a_devList)
{
  static int m_matRenderTimes = 0;

  IHydraNetPluginAPI* pImpl = nullptr;
  DWORD ticks = GetTickCount();

  std::stringstream ss;
  ss << ticks;

  std::string imageName   = std::string("HydraHDRImage_") + ss.str();
  std::string messageName = std::string("HydraMessageShmem_") + ss.str();
  std::string guiName     = std::string("HydraGuiShmem_") + ss.str();

  std::ostream* logPtr = nullptr;

  if (!inMatEditor)
  {
    if (!g_logMain.is_open())
      g_logMain.open("C:/[Hydra]/logs/plugin_log.txt");
    logPtr = &g_logMain;
    pImpl  = new PluginShmemPipe(imageName.c_str(), messageName.c_str(), guiName.c_str(), renderWidth, renderHeight, "main", a_devList, logPtr);
  }
  else // if in matEditor
  {
    if (!g_logMtl.is_open())
      g_logMtl.open("C:/[Hydra]/logs/material_log.txt");
    logPtr = &g_logMtl;

    if ((m_matRenderTimes != 0) && (m_matRenderTimes % 40 == 0)) // restart mtl render process each 32 render to prevent mem leaks
    {
      //hydraRender_mk3::plugin_log.Print("restart material render");
      if (g_pMaterialRenderConnect != nullptr)
        g_pMaterialRenderConnect->stopAllRenderProcesses();

      g_materialProcessStart = false;
    }

    pImpl = g_pMaterialRenderConnect;

    if (pImpl == nullptr)
    {
      g_pMaterialRenderConnect = new PluginShmemPipe(imageName.c_str(), messageName.c_str(), guiName.c_str(), 512, 512, "material", a_devList, logPtr);
      pImpl = g_pMaterialRenderConnect;
    }

    m_matRenderTimes++;
  }

  if (pImpl->hasConnection())
    return pImpl;
  else
  {
    delete pImpl;
    return nullptr;
  }

}

struct SharedBufferInfo2
{
  enum LAST_WRITER { WRITER_HYDRA_GUI = 0, WRITER_HYDRA_SERVER = 1 };

  unsigned int xmlBytesize;
  unsigned int lastWriter;
  unsigned int writerCounter;
  unsigned int dummy;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool isFileExist(const char *fileName)
{
  std::ifstream infile(fileName);
  return infile.good();
}

const int GUI_FILE_SIZE = 32768;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// PluginShmemPipe ///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PluginShmemPipe::PluginShmemPipe(const char* imageFileName, const char* messageFileName, const char* guiFileName, int width, int height, const char* connectionType, const std::vector<int>& a_devIdList, std::ostream* a_pLog) : m_hydraServerStarted(false), m_staticConnection(false), m_pLog(a_pLog)
{
  pImageA = nullptr;
  pImageB = nullptr;
  m_bvhMutex = NULL;
  m_mdDeviceList.clear();
  m_mdProcessList.clear();

  m_connectionType = connectionType;
  m_mdDeviceList = a_devIdList;

  if (m_connectionType == "main")
  {
    m_multiDevMode = true; // to enable specular filter

    m_mdProcessList.resize(m_mdDeviceList.size());
    m_bvhMutex = CreateMutexA(NULL, FALSE, "hydraBVHMutex");

    CreateConnectionMainType(imageFileName, messageFileName, guiFileName, width, height);
  }
  else
  {
    m_multiDevMode = false;
    CreateConnectionMaterialType(imageFileName, messageFileName, guiFileName, width, height);
  }

}

void PluginShmemPipe::CreateConnectionMainType(const char* imageFileName, const char* messageFileName, const char* guiFileName, int width, int height)
{
  m_staticConnection = false;

  m_hImageFile = NULL; // CreateFileMappingA(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, (width + 16)*(height + 16)*sizeof(float) * 4 + 1024, imageFileName);
  m_hFile      = CreateFileMappingA(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, 1024 + sizeof(int) * 4, messageFileName);
  m_hGuiFile   = CreateFileMappingA(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, GUI_FILE_SIZE, guiFileName);

  m_shmem      = (char*)MapViewOfFile(m_hFile, FILE_MAP_WRITE | FILE_MAP_READ, 0, 0, 0);
  m_imageShmem = NULL; // (char*)MapViewOfFile(m_hImageFile, FILE_MAP_WRITE | FILE_MAP_READ, 0, 0, 0);
  m_guiShmem   = (char*)MapViewOfFile(m_hGuiFile, FILE_MAP_WRITE | FILE_MAP_READ, 0, 0, 0);

  if (m_pLog != nullptr)
  {
    if (m_shmem == NULL || m_guiShmem == NULL)
    {
      (*m_pLog) << "[syscall failed]: CreateConnectionMainType() -> CreateFileMappingA or MapViewOfFile: " << std::endl;
      (*m_pLog) << "m_shmem    = " << m_shmem << std::endl;
      (*m_pLog) << "m_guiShmem = " << m_guiShmem << std::endl;
    }
  }

  // init gui mem
  //
  SharedBufferInfo2* pInfo2 = (SharedBufferInfo2*)m_guiShmem;

  pInfo2->xmlBytesize = 0;
  pInfo2->lastWriter = 0;
  pInfo2->writerCounter = 0;
  pInfo2->dummy = 0xFAFAFAFA; // magic end of the structure in the buffer

  // remember params
  //
  m_guiFileName = guiFileName;
  m_messageFileName = messageFileName;
  m_imageFileName = imageFileName;

  m_width = width;
  m_height = height;
}

void PluginShmemPipe::CreateConnectionMaterialType(const char* imageFileName, const char* messageFileName, const char* guiFileName, int width, int height)
{
  m_staticConnection = true;

  m_hImageFile = CreateFileMappingA(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, 1024, imageFileName);
  m_hFile      = CreateFileMappingA(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, 1024 + sizeof(int) * 4, messageFileName);
  m_hGuiFile   = CreateFileMappingA(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, GUI_FILE_SIZE, guiFileName);


  if (m_hImageFile == NULL || m_hFile == NULL || m_hGuiFile == NULL)
  {
    if (m_pLog != nullptr)
      (*m_pLog) << "CreateConnectionMaterialType(): CreateFileMappingA failed" << std::endl;
  }

  m_imageShmem = (char*)MapViewOfFile(m_hImageFile, FILE_MAP_WRITE | FILE_MAP_READ, 0, 0, 0);
  m_shmem      = (char*)MapViewOfFile(m_hFile, FILE_MAP_WRITE | FILE_MAP_READ, 0, 0, 0);
  m_guiShmem   = (char*)MapViewOfFile(m_hGuiFile, FILE_MAP_WRITE | FILE_MAP_READ, 0, 0, 0);

  if (m_imageShmem == NULL || m_shmem == NULL || m_guiShmem == NULL)
  {
    if (m_pLog != nullptr)
      (*m_pLog) << "CreateConnectionMaterialType(): MapViewOfFile failed" << std::endl;
  }

  // put initial data
  //
  SharedBufferDataInfo* pInfo = bufferInfo();
  SharedBufferDataInfo* pImageInfo = imageBufferInfo();

  pImageInfo->read = 1;
  pImageInfo->width = 0; // write width
  pImageInfo->height = 0; // write height
  pImageInfo->written = 0;

  if (pInfo->written == HYDRA_SERVER_ID || pInfo->written == HYDRA_PLUGIN_ID)
  {
    memset(bufferData(), 0, 1024);
    pInfo->written = HYDRA_PLUGIN_ID;
    pInfo->read = 1;
  }

  SharedBufferInfo2* pInfo2 = (SharedBufferInfo2*)m_guiShmem;

  pInfo2->xmlBytesize = 0;
  pInfo2->lastWriter = 0;
  pInfo2->writerCounter = 0;
  pInfo2->dummy = 0xFAFAFAFA; // magic end of the structure in the buffer

  // load
  //
  std::ifstream fin("C:\\[Hydra]\\pluginFiles\\material\\settings.xml");
  std::string str((std::istreambuf_iterator<char>(fin)), std::istreambuf_iterator<char>());

  char* data = (char*)(m_guiShmem)+sizeof(SharedBufferInfo2);
  memset(data, 0, GUI_FILE_SIZE - sizeof(SharedBufferInfo2));
  memcpy(data, str.c_str(), str.length());

  // remember params
  //
  m_guiFileName = guiFileName;
  m_messageFileName = messageFileName;
  m_imageFileName = imageFileName;

  m_width = width;
  m_height = height;
}


void PluginShmemPipe::runAllRenderProcesses(RenderProcessRunParams a_params, const std::vector<HydaRenderDevice>& a_devList)
{
  std::ostream* outp = m_pLog;

  // restore params
  //
  bool a_showCmd           = a_params.showConsole;
  bool a_normalPriorityCPU = a_params.normalPriorityCPU;
  bool a_debug             = a_params.debug;

  const char* guiFileName     = m_guiFileName.c_str();
  const char* messageFileName = m_messageFileName.c_str();
  const char* imageFileName   = m_imageFileName.c_str();

  int width = m_width;
  int height = m_height;

  m_mdProcessList.resize(a_devList.size());

  if (m_connectionType == "main")
  {
    ZeroMemory(&m_hydraStartupInfo, sizeof(STARTUPINFOA));
    ZeroMemory(&m_hydraProcessInfo, sizeof(PROCESS_INFORMATION));

    m_hydraStartupInfo.cb = sizeof(STARTUPINFO);
    m_hydraStartupInfo.dwFlags     = STARTF_USESHOWWINDOW; // CREATE_NEW_CONSOLE // DETACHED_PROCESS
    m_hydraStartupInfo.wShowWindow = SW_SHOWMINNOACTIVE;

    const char* hydraPath = "C:\\[Hydra]\\bin\\hydra.exe";
    if (a_params.customExePath != "")
      hydraPath = a_params.customExePath.c_str();

    if (!isFileExist(hydraPath))
    {
      m_hydraServerStarted = false;
    }
    else
    {
      std::stringstream ss;
      ss << "xxx yyy zzz " << " -gui_buffer_name " << guiFileName << " -image_buffer_name " << imageFileName << " -message_buffer_name " << messageFileName;

      std::stringstream ss2;
      ss2 << GetTickCount();

      std::string imgA = "hydraImageA_" + ss2.str();
      std::string imgB = "hydraImageB_" + ss2.str();

      std::string mutexA = "hydraImageAMutex_" + ss2.str();
      std::string mutexB = "hydraImageBMutex_" + ss2.str();

      const int layersNum = a_params.enableMLT ? 2 : 1;
      const int enableMLT = a_params.enableMLT ? 1 : 0;

      pImageA = new ImageZ(imgA.c_str(), mutexA.c_str(), width, height, outp, layersNum);
      pImageB = new ImageZ(imgB.c_str(), mutexB.c_str(), width, height, outp, layersNum);

      std::string tempStr = std::string(" -multidevicemode 1 -imageB ") + imgB + " -mutexB " + mutexB + " -imageA " + imgA + " -mutexA " + mutexA;

      ss << tempStr.c_str();
      ss << " -nowindow 1 -enable_mlt " << enableMLT << " ";
      ss << a_params.customExeArgs.c_str();
      if(a_params.customLogFold != "")
        ss << "-logdir \"" << a_params.customLogFold.c_str() << "\" ";

      if (pImageA != nullptr)
        pImageA->SendMsg("-node_t A -sid 0 -layer wait -action wait");

      int deviceId = m_mdDeviceList.size() == 0 ? -1 : m_mdDeviceList[0];
      int engineType = 1;
      int liteMode = false;

      std::string basicCmd = ss.str();

      m_hydraServerStarted = true;
      std::ofstream fout("C:\\[Hydra]\\pluginFiles\\zcmd.txt");

      for (size_t i = 0; i < m_mdDeviceList.size(); i++)
      {
        DWORD dwCreationFlags = a_showCmd ? 0 : CREATE_NO_WINDOW;

        int devId = m_mdDeviceList[i];
        if (isTargetDevIdACPU(devId, a_devList))
        {
          if (isTargetDevIdAHydraCPU(devId, a_devList))
          {
            devId *= -1;
            if (m_mdDeviceList.size() != 1 && !a_normalPriorityCPU)
              dwCreationFlags |= BELOW_NORMAL_PRIORITY_CLASS; // is CPU is the only one device, use normal priority, else use background mode
          }
        }

        if(a_showCmd)
          dwCreationFlags |= CREATE_NEW_CONSOLE;

        std::stringstream ss3;

        if (engineType == 1)
          ss3 << " -cl_enable 1 -cl_device_id " << devId << " -cl_lite_core " << liteMode;
        else
          ss3 << " -cl_enable 0 -cl_device_id " << devId;

        std::string cmdFull = basicCmd + ss3.str();

        ZeroMemory(&m_mdProcessList[i], sizeof(PROCESS_INFORMATION));
        if (!a_debug)
        {
          m_hydraServerStarted = m_hydraServerStarted && CreateProcessA(hydraPath, (LPSTR)cmdFull.c_str(), NULL, NULL, FALSE, dwCreationFlags, NULL, NULL, &m_hydraStartupInfo, &m_mdProcessList[i]);
          if (!m_hydraServerStarted && outp != nullptr)
          {
            (*outp) << "[syscall failed]: runAllRenderProcesses->(m_connectionType == 'main')->CreateProcessA " << std::endl;
          }
        }

        fout << cmdFull.c_str() << std::endl;
      }

      fout.close();

    }
  }
  else if (!g_materialProcessStart)
  {
    ZeroMemory(&m_hydraStartupInfo, sizeof(STARTUPINFOA));
    ZeroMemory(&m_hydraProcessInfo, sizeof(PROCESS_INFORMATION));

    m_hydraStartupInfo.cb = sizeof(STARTUPINFO);
    m_hydraStartupInfo.dwFlags = STARTF_USESHOWWINDOW;
    m_hydraStartupInfo.wShowWindow = SW_SHOWMINNOACTIVE;


    const char* hydraPath = "C:\\[Hydra]\\bin\\hydra.exe";
    const char* matScenePath = "C:\\[Hydra]\\pluginFiles\\material\\";

    if (!isFileExist(hydraPath))
    {
      m_hydraServerStarted = false;
    }
    else
    {
      ZeroMemory(&m_hydraStartupInfo, sizeof(STARTUPINFOA));
      ZeroMemory(&m_hydraProcessInfo, sizeof(PROCESS_INFORMATION));

      m_hydraStartupInfo.cb = sizeof(STARTUPINFO);
      m_hydraStartupInfo.dwFlags = STARTF_USESHOWWINDOW;
      m_hydraStartupInfo.wShowWindow = SW_SHOWMINNOACTIVE;

      std::stringstream ss;
      ss << hydraPath << " " << matScenePath << "scene.vsgf " << matScenePath << "hydra_profile_generated.xml " << matScenePath << "settings.xml -out " << matScenePath << "z_material.imagef4 -outimagef4 1 -nobvhcache 1 -inmaterialditor 1 -wait 1" << " -gui_buffer_name " << guiFileName << " -image_buffer_name " << imageFileName << " -message_buffer_name " << messageFileName;

      if (1)
      {
        std::stringstream ss2;
        ss2 << GetTickCount();

        std::string imgA = "hydraImageA_" + ss2.str();
        std::string imgB = "hydraImageB_" + ss2.str();

        std::string mutexA = "hydraImageAMutex_" + ss2.str();
        std::string mutexB = "hydraImageBMutex_" + ss2.str();

        pImageA = new ImageZ(imgA.c_str(), mutexA.c_str(), width, height, outp);
        pImageB = new ImageZ(imgB.c_str(), mutexB.c_str(), width, height, outp);

        std::string tempStr = std::string(" -multidevicemode 1 -imageB ") + imgB + " -mutexB " + mutexB + " -imageA " + imgA + " -mutexA " + mutexA;
        ss << tempStr.c_str();
      }

      ss << " -nowindow 1 ";

      int deviceId   = a_devList.at(0).id; // (hydraRender_mk3::m_lastRendParams.device_id.size() == 0) ? -1 : hydraRender_mk3::m_lastRendParams.device_id.at(0); // pRendParams->
      int engineType = 1;
      int liteMode   = false;

      DWORD dwCreationFlags = a_showCmd ? 0 : CREATE_NO_WINDOW;

      if (isTargetDevIdACPU(deviceId, a_devList))
      {
        if (isTargetDevIdAHydraCPU(deviceId, a_devList))
          deviceId *= -1;
        //dwCreationFlags |= BELOW_NORMAL_PRIORITY_CLASS;
      }

      if (engineType == 1)
        ss << " -cl_enable 1 -cl_device_id " << deviceId << " -cl_lite_core " << liteMode;
      else
        ss << " -cl_enable 0 -cl_device_id " << deviceId << " -cl_lite_core ";

      if (a_params.compileShaders)
        ss << " -compile_shaders 1 ";

      m_hydraServerStarted = CreateProcessA("C:\\[Hydra]\\bin\\hydra.exe", (LPSTR)ss.str().c_str(), NULL, NULL, FALSE, dwCreationFlags, NULL, NULL, &m_hydraStartupInfo, &m_hydraProcessInfo); // CREATE_NEW_CONSOLE, CREATE_NO_WINDOW
      if (!m_hydraServerStarted && outp != nullptr)
        (*outp) << "[syscall failed]: runAllRenderProcesses->(m_connectionType == 'material')->CreateProcessA " << std::endl;

      std::ofstream fout("C:\\[Hydra]\\pluginFiles\\material\\cmd.txt");
      fout << ss.str().c_str() << std::endl;
      fout.close();
    }

    if (a_params.compileShaders)
      g_materialProcessStart = false;
    else
      g_materialProcessStart = true;

    g_materialProcessInfo = m_hydraProcessInfo;
  }

}


void PluginShmemPipe::stopAllRenderProcesses()
{

  if (m_multiDevMode && m_hydraServerStarted)
  {
    for (auto i = 0; i < m_mdProcessList.size(); i++)
    {
      if (m_mdProcessList[i].hProcess == 0 || m_mdProcessList[i].hProcess == INVALID_HANDLE_VALUE)
        continue;

      DWORD exitCode;
      GetExitCodeProcess(m_mdProcessList[i].hProcess, &exitCode);

      if (exitCode == STILL_ACTIVE)
        Sleep(100);

      GetExitCodeProcess(m_mdProcessList[i].hProcess, &exitCode);

      if (exitCode == STILL_ACTIVE)
        TerminateProcess(m_mdProcessList[i].hProcess, NO_ERROR);
    }
  }
  else if (m_hydraServerStarted && m_hydraProcessInfo.hProcess != 0 && m_hydraProcessInfo.hProcess != INVALID_HANDLE_VALUE)
  {
    // ask process for exit
    //
    DWORD exitCode;
    GetExitCodeProcess(m_hydraProcessInfo.hProcess, &exitCode);

    if (exitCode == STILL_ACTIVE)
      Sleep(100);

    GetExitCodeProcess(m_hydraProcessInfo.hProcess, &exitCode);

    if (exitCode == STILL_ACTIVE)
      TerminateProcess(m_hydraProcessInfo.hProcess, NO_ERROR);
  }


}

PluginShmemPipe::~PluginShmemPipe()
{
  stopAllRenderProcesses();

  if (m_bvhMutex != NULL)
    CloseHandle(m_bvhMutex);

  delete pImageA; pImageA = nullptr;
  delete pImageB; pImageB = nullptr;

  UnmapViewOfFile(m_hImageFile); CloseHandle(m_hImageFile); m_hImageFile = INVALID_HANDLE_VALUE;
  UnmapViewOfFile(m_hFile);      CloseHandle(m_hFile);      m_hFile = INVALID_HANDLE_VALUE;
  UnmapViewOfFile(m_hGuiFile);   CloseHandle(m_hGuiFile);   m_hGuiFile = INVALID_HANDLE_VALUE;
}


bool  PluginShmemPipe::hasConnection() const
{
  return (m_hFile != NULL) && (m_hGuiFile != NULL);
}



void PluginShmemPipe::updatePresets(const char* a_presetsXML)
{
  SharedBufferInfo2* pInfo = (SharedBufferInfo2*)m_guiShmem;
  char* data = m_guiShmem + sizeof(SharedBufferInfo2);

  //memset(m_guiShmem, 0, GUI_FILE_SIZE);
  strcpy(data, a_presetsXML);

  pInfo->xmlBytesize = (unsigned int)strlen(a_presetsXML);
  pInfo->lastWriter = 0;
  pInfo->dummy = 0xFAFAFAFA; // magic end of the structure in the buffer
  pInfo->writerCounter = pInfo->writerCounter + 2;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


bool isTargetDevIdACPU(int a_devId, const std::vector<HydaRenderDevice>& a_devList)
{
  bool res = false;

  for (size_t i = 0; i < a_devList.size(); i++)
  {
    if (a_devList[i].id == a_devId)
    {
      res = a_devList[i].isCPU;
      break;
    }
  }

  return res;
}


bool isTargetDevIdAHydraCPU(int a_devId, const std::vector<HydaRenderDevice>& a_devList)
{
  bool res = false;

  for (size_t i = 0; i < a_devList.size(); i++)
  {
    if (a_devList[i].id == a_devId)
    {
      res = (a_devList[i].name == L"Hydra CPU");
      break;
    }
  }

  return res;
}

// std::wstring HydraInstallPathW() { return std::wstring(L"C:\\[Hydra]\\"); }


std::string ws2s(const std::wstring& s)
{
  int len;
  int slength = (int)s.length() + 1;
  len = WideCharToMultiByte(CP_ACP, 0, s.c_str(), slength, 0, 0, 0, 0);
  char* buf = new char[len];
  WideCharToMultiByte(CP_ACP, 0, s.c_str(), slength, buf, len, 0, 0);
  std::string r(buf);
  delete[] buf;
  return r;
}

std::wstring s2ws(const std::string& s)
{
  int len;
  int slength = (int)s.length() + 1;
  len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
  wchar_t* buf = new wchar_t[len];
  MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
  std::wstring r(buf);
  delete[] buf;
  return r;
}

std::vector<HydaRenderDevice> InitDeviceList(int mode)
{

#ifdef SILENT_DEVICE_ID

  HydaRenderDevice dev;
  dev.driverName = L"MyTestDriver";
  dev.name = L"SomeTestDevice";
  dev.id = 0;
  dev.isCPU = false;

  std::vector<HydaRenderDevice> devs;
  devs.push_back(dev);
  return devs;

#endif

  std::vector<HydaRenderDevice> result;

  STARTUPINFOA m_hydraStartupInfo;
  PROCESS_INFORMATION m_hydraProcessInfo;

  ZeroMemory(&m_hydraStartupInfo, sizeof(STARTUPINFOA));
  ZeroMemory(&m_hydraProcessInfo, sizeof(PROCESS_INFORMATION));
  const char* hydraPath = "C:\\[Hydra]\\bin2\\hydra.exe";

  m_hydraStartupInfo.cb = sizeof(STARTUPINFO);
  m_hydraStartupInfo.dwFlags = STARTF_USESHOWWINDOW;
  m_hydraStartupInfo.wShowWindow = SW_SHOWMINNOACTIVE;

  DeleteFileA("C:\\[Hydra]\\logs\\devlist.txt");

  std::stringstream ss;
  ss << " -cl_list_devices 1 ";

  CreateProcessA(hydraPath, (LPSTR)ss.str().c_str(), NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL, &m_hydraStartupInfo, &m_hydraProcessInfo);
  WaitForSingleObject(m_hydraProcessInfo.hProcess, INFINITE);
  CloseHandle(m_hydraProcessInfo.hProcess);
  CloseHandle(m_hydraProcessInfo.hThread);

  if (isFileExist("C:\\[Hydra]\\logs\\devlist.txt"))
  {
    hr_mkdir(L"C:\\[Hydra]\\logs\\");
    std::wifstream in;
    in.open("C:\\[Hydra]\\logs\\devlist.txt");
    std::wstring dev_id, dev_name, dev_driver, dev_type;
    std::wstring str;
    std::vector<std::wstring> device;

    std::wstring wordsToRemove[] = { L"Intel(R) Core(TM) ", L"OpenCL " };

    while (std::getline(in, str))
      device.push_back(str);

    in.close();

    for (auto i = 0; i < device.size(); ++i)
    {
      str = device.at(i);
      //MessageBoxW(NULL, str.c_str(), L"", 0);

      std::size_t found = str.find_first_of(';');
      std::size_t found2 = str.find_first_of(';', found + 1);
      std::size_t found3 = str.find_first_of(';', found2 + 1);

      dev_id = str.substr(0, found);

      if (found == std::wstring::npos || found2 == std::wstring::npos || found3 == std::wstring::npos)
      {
        //rend->plugin_log.Print("InitDeviceList: bad device string");
        continue;
      }

      dev_name = str.substr(found + 2, found2 - 3);
      dev_driver = str.substr(found2 + 2, found3 - found2 - 2);
      dev_type = str.substr(found3 + 2, str.find_last_of(';'));


      for (auto word : wordsToRemove)
      {
        auto pos1 = dev_name.find(word);
        if (pos1 != std::wstring::npos)
          dev_name.replace(pos1, pos1 + word.size(), L"");

        auto pos2 = dev_driver.find(word);
        if (pos2 != std::wstring::npos)
          dev_driver.replace(pos2, pos2 + word.size(), L"");
      }

      HydaRenderDevice devInfo;
      devInfo.name = dev_name;
      devInfo.driverName = dev_driver;
      devInfo.id = stoi(dev_id);
      devInfo.isCPU = (dev_type.find(L"CPU") != std::string::npos);
      result.push_back(devInfo);
    }

  }
  else
  {
    HydaRenderDevice devInfo;
    devInfo.name       = L"Hydra CPU";
    devInfo.driverName = L"Pure C/C++";
    devInfo.id         = -1;
    devInfo.isCPU      = true;
    result.push_back(devInfo);
  }

  return result;
}


std::vector<float> LoadGBuffer(const std::wstring& a_folderPath, int* pW, int* pH)
{
  std::ifstream fin(a_folderPath.c_str(), std::ios::binary);

  if (!fin.is_open())
    return std::vector<float>();
  
  int wh[2] = { 0, 0 };
  fin.read((char*)wh, 2 * sizeof(int));

  if(pW != nullptr) (*pW) = wh[0];
  if(pH != nullptr) (*pH) = wh[1];

  std::vector<float> data(wh[0] * wh[1] * 4);

  if (data.size() == 0)
    return data;

  fin.read((char*)&data[0], wh[0] * wh[1] * 4 * sizeof(float));
  fin.close();
  return data;
}

