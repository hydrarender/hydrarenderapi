#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include "pugixml.hpp"

#include "HydraExport.h" // geometry 

std::wstring ToWString(int i)
{
  std::wstringstream strOut;
  strOut << i;
  return strOut.str();
}

using pugi::xml_node;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
\brief a simple example of creating lambert material description
\param matNode - xml node to <material>
\param color   - material color, for example "0 1 0" is green
\param texture - material texture
\param id      - a global id of material in scene; note that you do't have to manage id's in new API manually
\param name    - material name 

*/
void RecordLambertMaterial(xml_node matNode, const std::wstring& color, const std::wstring& texture, int id, const std::wstring& name)
{
  const std::wstring idStr = ToWString(id);

  matNode.append_attribute(L"type").set_value(idStr.c_str());  // type="hydra"; this indicates legacy uber material;
  matNode.append_attribute(L"name").set_value(name.c_str());   // name="someName"
  matNode.append_attribute(L"maxid").set_value(idStr.c_str()); // maxid="someId"


  xml_node innerNode = matNode.append_child(L"hydra");         // create <material> -> <hydra>; this indicates legacy uber material;
                                                               // This iternal node is legacy, but we will support it in future.
                                                               // For current build you must add this meaningless node. Sorry :(

  xml_node diffNode = innerNode.append_child(L"diffuse");      // create <hydra> -> <diffuse>

  diffNode.append_child(L"color").text().set(color.c_str());     // put color   to <diffuse> -> <color>
  diffNode.append_child(L"texture").text().set(texture.c_str()); // put texture to <diffuse> -> <texture>
}

/**
\brief a simple example of creating area light
\param lightNode 
\param color   
\param sizeX 
\param sizeY      
\param name    

*/
void RecordSimpleAreaLight(xml_node lightNode, const std::wstring& color, int id, const std::wstring& name)
{
  const std::wstring idStr = ToWString(id);

  const std::wstring shape = L"plane";
  const std::wstring distr = L"diffuse";

  lightNode.append_attribute(L"type").set_value(shape.c_str());         // type="plane";  this indicates light geometry type; currently must be the same as shape;
  lightNode.append_attribute(L"shape").set_value(shape.c_str());        // shape="plane"; this indicates light shape;         currently must be the same as type;
  lightNode.append_attribute(L"distribution").set_value(distr.c_str()); // distribution="diffuse"

  lightNode.append_attribute(L"name").set_value(name.c_str());       // name="someName" 
  lightNode.append_attribute(L"id").set_value(idStr.c_str());        // id="someId" 

  //////////////////////////////////////////////////////////////////////////////////////////////////
  //
  xml_node generalNode = lightNode.append_child(L"general");           // this is legacy node; It must copy attribute values in general.
                                                                       // we will support it in future also if attributes are empty.
                                                                       // For current build you must add this copyes. Sorry :(

  generalNode.append_child(L"type").text().set(shape.c_str());         // copy shape/type
  generalNode.append_child(L"distribution").text().set(distr.c_str()); // copy distr
  //
  //////////////////////////////////////////////////////////////////////////////////////////////////

  lightNode.append_child(L"position").text().set(L"0 2 0"); 
  lightNode.append_child(L"direction").text().set(L"0 -1 0");             // default area light have direction (0 -1 0) and this is related to identity rotation matrix
  lightNode.append_child(L"matrix_rot").text().set(L"1 0 0 0 1 0 0 0 1"); // identity matrix

  xml_node sizeNode = lightNode.append_child(L"size");

  sizeNode.append_child(L"Half-length").text().set(L"0.5");
  sizeNode.append_child(L"Half-width").text().set(L"0.5");

  xml_node intensity = lightNode.append_child(L"intensity");

  intensity.append_child(L"color").text().set(color.c_str());
  intensity.append_child(L"multiplier").text().set(L"1.0");
}

void RecordEnvLight(xml_node lightNode, int id, const std::wstring& name)
{
  const std::wstring idStr = ToWString(id);
  const std::wstring shape = L"sky";
  const std::wstring distr = L"anyshit";

  lightNode.append_attribute(L"type").set_value(shape.c_str());         // type="plane";  this indicates light geometry type; currently must be the same as shape;
  lightNode.append_attribute(L"shape").set_value(shape.c_str());        // shape="plane"; this indicates light shape;         currently must be the same as type;
  lightNode.append_attribute(L"distribution").set_value(distr.c_str()); // distribution="diffuse"

  lightNode.append_attribute(L"name").set_value(name.c_str());       // name="someName" 
  lightNode.append_attribute(L"id").set_value(idStr.c_str());        // id="someId" 

  //////////////////////////////////////////////////////////////////////////////////////////////////
  //
  xml_node generalNode = lightNode.append_child(L"general");           // this is legacy node; It must copy attribute values in general.
  // we will support it in future also if attributes are empty.
  // For current build you must add this copyes. Sorry :(

  generalNode.append_child(L"type").text().set(shape.c_str());         // copy shape/type
  generalNode.append_child(L"distribution").text().set(distr.c_str()); // copy distr
  //
  //////////////////////////////////////////////////////////////////////////////////////////////////

  xml_node intensity = lightNode.append_child(L"intensity");

  intensity.append_child(L"color").text().set(L"0.75 0.5 0.75");
  intensity.append_child(L"multiplier").text().set(L"1.0");
}

/**
\brief a simple example of creating area light
\param lightNode
\param color
\param sizeX
\param sizeY
\param name

*/
void RecordCamera(xml_node camNode, const std::wstring& name)
{
  camNode.append_attribute(L"type").set_value(L"default_camera");
  camNode.append_attribute(L"id").set_value(L"0");
  camNode.append_attribute(L"name").set_value(name.c_str());

  camNode.append_child(L"fov").text().set(L"45.0");
  camNode.append_child(L"nearClipPlane").text().set(L"0.02");
  camNode.append_child(L"farClipPlane").text().set(L"100.0");

  camNode.append_child(L"up").text().set(L"0 1 0");
  camNode.append_child(L"position").text().set(L"0 1 4.5");
  camNode.append_child(L"look_at").text().set(L"0 1 0");

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
\brief create xml file with materials and lights description
\param a_fileName - path to xml file

*/

void create_profile(const std::wstring& a_fileName)
{
  pugi::xml_document doc;

  xml_node mainNode  = doc.append_child(L"COLLADA");                    // the main node just like in COLLADA
                                                                        
  mainNode.append_attribute(L"profile").set_value(L"HydraProfile");     // profile="HydraProfile"

  xml_node materials_lib = mainNode.append_child(L"library_materials"); // create <COLLADA> -> <library_materials>
  xml_node lights_lib    = mainNode.append_child(L"library_lights");    // create <COLLADA> -> <library_lights>
  xml_node cameras_lib   = mainNode.append_child(L"library_cameras");   // create <COLLADA> -> <library_cameras>

  // add 2 simple materials to materials_lib
  //
  xml_node mat1 = materials_lib.append_child(L"material");              // create <library_materials> -> <material>; note that you don't have to do this in new API _manually_
  xml_node mat2 = materials_lib.append_child(L"material");              // create <library_materials> -> <material>; note that you don't have to do this in new API _manually_

  RecordLambertMaterial(mat1, L"0.5 0.5 0.5", L"texture1.bmp", 0, L"plane_mtl");
  RecordLambertMaterial(mat2, L"0.5 1.0 0.5", L"",             1, L"cube_mlt");

  // add 2 lights to lights_lib
  //
  xml_node light1 = lights_lib.append_child(L"light");                  // create <library_materials> -> <light>; note that you don't have to do this in new API _manually_
  RecordSimpleAreaLight(light1, L"10 10 10", 0, L"MyAreaLight");
  
  //xml_node light2 = lights_lib.append_child(L"light");                // create <library_materials> -> <light>; note that you don't have to do this in new API _manually_
  //RecordEnvLight(light2, 1, L"Environment");

  // add 1 camera to cameras_lib
  //
  xml_node cam = cameras_lib.append_child(L"camera");                   // create <library_cameras> -> <camera>;

  RecordCamera(cam, L"MyCamera");

  doc.save_file(a_fileName.c_str(), L"  ");
}

void create_settings(const std::wstring& a_fileName)
{
  // ... you can take some settings file from "C:\[Hydra]\pluginFiles\settings.xml" after you render scene with some presets
  // this file is used for command line run 
}

struct SimpleHydraMesh //< this is plain data for mesh 
{
  std::vector<float>    allVerticesPos;       //< float4
  std::vector<float>    allVerticesNorm;      //< float4
  std::vector<float>    allVerticesTexCoord;  //< float2
  std::vector<uint32_t> allTriIndices;        //< size of 3*triNum
  std::vector<uint32_t> allmatIndices;        //< size of 1*triNum
};


void AppendShapeCube(SimpleHydraMesh* pMesh)
{
  if (pMesh == nullptr)
    return;

  uint32_t numberVertices = 24;
  uint32_t numberIndices  = 36;

  float cubeVertices[] =
  {
    -1.0f, -1.0f, -1.0f, +1.0f,
    -1.0f, -1.0f, +1.0f, +1.0f,
    +1.0f, -1.0f, +1.0f, +1.0f,
    +1.0f, -1.0f, -1.0f, +1.0f,
    -1.0f, +1.0f, -1.0f, +1.0f,
    -1.0f, +1.0f, +1.0f, +1.0f,
    +1.0f, +1.0f, +1.0f, +1.0f,
    +1.0f, +1.0f, -1.0f, +1.0f,
    -1.0f, -1.0f, -1.0f, +1.0f,
    -1.0f, +1.0f, -1.0f, +1.0f,
    +1.0f, +1.0f, -1.0f, +1.0f,
    +1.0f, -1.0f, -1.0f, +1.0f,
    -1.0f, -1.0f, +1.0f, +1.0f,
    -1.0f, +1.0f, +1.0f, +1.0f,
    +1.0f, +1.0f, +1.0f, +1.0f,
    +1.0f, -1.0f, +1.0f, +1.0f,
    -1.0f, -1.0f, -1.0f, +1.0f,
    -1.0f, -1.0f, +1.0f, +1.0f,
    -1.0f, +1.0f, +1.0f, +1.0f,
    -1.0f, +1.0f, -1.0f, +1.0f,
    +1.0f, -1.0f, -1.0f, +1.0f,
    +1.0f, -1.0f, +1.0f, +1.0f,
    +1.0f, +1.0f, +1.0f, +1.0f,
    +1.0f, +1.0f, -1.0f, +1.0f
  };

  float cubeNormals[] =
  {
    0.0f, -1.0f, 0.0f, +1.0f,
    0.0f, -1.0f, 0.0f, +1.0f,
    0.0f, -1.0f, 0.0f, +1.0f,
    0.0f, -1.0f, 0.0f, +1.0f,
    0.0f, +1.0f, 0.0f, +1.0f,
    0.0f, +1.0f, 0.0f, +1.0f,
    0.0f, +1.0f, 0.0f, +1.0f,
    0.0f, +1.0f, 0.0f, +1.0f,
    0.0f, 0.0f, -1.0f, +1.0f,
    0.0f, 0.0f, -1.0f, +1.0f,
    0.0f, 0.0f, -1.0f, +1.0f,
    0.0f, 0.0f, -1.0f, +1.0f,
    0.0f, 0.0f, +1.0f, +1.0f,
    0.0f, 0.0f, +1.0f, +1.0f,
    0.0f, 0.0f, +1.0f, +1.0f,
    0.0f, 0.0f, +1.0f, +1.0f,
    -1.0f, 0.0f, 0.0f, +1.0f,
    -1.0f, 0.0f, 0.0f, +1.0f,
    -1.0f, 0.0f, 0.0f, +1.0f,
    -1.0f, 0.0f, 0.0f, +1.0f,
    +1.0f, 0.0f, 0.0f, +1.0f,
    +1.0f, 0.0f, 0.0f, +1.0f,
    +1.0f, 0.0f, 0.0f, +1.0f,
    +1.0f, 0.0f, 0.0f, +1.0f
  };

  float cubeTexCoords[] =
  {
    0.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
    1.0f, 0.0f,
    1.0f, 1.0f,
    0.0f, 1.0f,
    0.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
  };

  uint32_t cubeIndices[] =
  {
    0, 2, 1,
    0, 3, 2,
    4, 5, 6,
    4, 6, 7,
    8, 9, 10,
    8, 10, 11,
    12, 15, 14,
    12, 14, 13,
    16, 17, 18,
    16, 18, 19,
    20, 23, 22,
    20, 22, 21
  };

  // move and scale cube
  //
  for (uint32_t i = 0; i < numberVertices; i++)
  {
    cubeVertices[i * 4 + 0] = cubeVertices[i * 4 + 0] * 0.5f + 0.0f;
    cubeVertices[i * 4 + 1] = cubeVertices[i * 4 + 1] * 0.5f + 0.5f;
    cubeVertices[i * 4 + 2] = cubeVertices[i * 4 + 2] * 0.5f + 0.0f;
  }

  // remember old vertex buffer size
  //
  const uint32_t oldVertexNum = pMesh->allVerticesPos.size() / 4;

  pMesh->allVerticesPos.insert(pMesh->allVerticesPos.end(), cubeVertices, cubeVertices + numberVertices*4);             // append vertices
  pMesh->allVerticesNorm.insert(pMesh->allVerticesNorm.end(), cubeNormals, cubeNormals + numberVertices*4);             // append normals
  pMesh->allVerticesTexCoord.insert(pMesh->allVerticesTexCoord.end(), cubeTexCoords, cubeTexCoords + numberVertices*2); // append texture coordinates
  
  // now append triangle indices ...
  //
  for (uint32_t i = 0; i < numberIndices; i++)
    pMesh->allTriIndices.push_back(oldVertexNum + cubeIndices[i]);

  // append per triangle material id
  //
  for (uint32_t i = 0; i < (numberIndices / 3); i++)
    pMesh->allmatIndices.push_back(1);
}


void create_geometry(const std::wstring& a_fileName)
{
  const int Nvert = 4;

  SimpleHydraMesh mesh;

  mesh.allVerticesPos.resize(Nvert * 4);
  mesh.allVerticesNorm.resize(Nvert * 4);
  mesh.allVerticesTexCoord.resize(Nvert * 2);
  mesh.allTriIndices.resize(6);
  mesh.allmatIndices.resize(2);

  // plane pos, pos is float4
  //
  mesh.allVerticesPos[0 * 4 + 0] = -2.0f;
  mesh.allVerticesPos[0 * 4 + 1] = 0.0f;
  mesh.allVerticesPos[0 * 4 + 2] = -2.0f;
  mesh.allVerticesPos[0 * 4 + 3] = 1.0f; // must be 1
                     
  mesh.allVerticesPos[1 * 4 + 0] = 2.0f;
  mesh.allVerticesPos[1 * 4 + 1] = 0.0f;
  mesh.allVerticesPos[1 * 4 + 2] = -2.0f;
  mesh.allVerticesPos[1 * 4 + 3] = 1.0f; // must be 1                

  mesh.allVerticesPos[2 * 4 + 0] = 2.0f;
  mesh.allVerticesPos[2 * 4 + 1] = 0.0f;
  mesh.allVerticesPos[2 * 4 + 2] = 2.0f;
  mesh.allVerticesPos[2 * 4 + 3] = 1.0f; // must be 1
                     
  mesh.allVerticesPos[3 * 4 + 0] = -2.0f;
  mesh.allVerticesPos[3 * 4 + 1] = 0.0f;
  mesh.allVerticesPos[3 * 4 + 2] = 2.0f;
  mesh.allVerticesPos[3 * 4 + 3] = 1.0f; // must be 1

  // plane normals, norm is float4
  //
  mesh.allVerticesNorm[0 * 4 + 0] = 0.0f;
  mesh.allVerticesNorm[0 * 4 + 1] = 1.0f;
  mesh.allVerticesNorm[0 * 4 + 2] = 0.0f;
  mesh.allVerticesNorm[0 * 4 + 3] = 1.0f; // must be 1
                      
  mesh.allVerticesNorm[1 * 4 + 0] = 0.0f;
  mesh.allVerticesNorm[1 * 4 + 1] = 1.0f;
  mesh.allVerticesNorm[1 * 4 + 2] = 0.0f;
  mesh.allVerticesNorm[1 * 4 + 3] = 1.0f; // must be 1
                      
  mesh.allVerticesNorm[2 * 4 + 0] = 0.0f;
  mesh.allVerticesNorm[2 * 4 + 1] = 1.0f;
  mesh.allVerticesNorm[2 * 4 + 2] = 0.0f;
  mesh.allVerticesNorm[2 * 4 + 3] = 1.0f; // must be 1

  mesh.allVerticesNorm[3 * 4 + 0] = 0.0f;
  mesh.allVerticesNorm[3 * 4 + 1] = 1.0f;
  mesh.allVerticesNorm[3 * 4 + 2] = 0.0f;
  mesh.allVerticesNorm[3 * 4 + 3] = 1.0f; // must be 1

  // plane texture coords
  //
  mesh.allVerticesTexCoord[0 * 2 + 0] = 0.0f;
  mesh.allVerticesTexCoord[0 * 2 + 1] = 0.0f;

  mesh.allVerticesTexCoord[1 * 2 + 0] = 1.0f;
  mesh.allVerticesTexCoord[1 * 2 + 1] = 0.0f;

  mesh.allVerticesTexCoord[2 * 2 + 0] = 1.0f;
  mesh.allVerticesTexCoord[2 * 2 + 1] = 1.0f;

  mesh.allVerticesTexCoord[3 * 2 + 0] = 0.0f;
  mesh.allVerticesTexCoord[3 * 2 + 1] = 1.0f;

  // construct triangles
  //
  mesh.allTriIndices[0] = 0;
  mesh.allTriIndices[1] = 1;
  mesh.allTriIndices[2] = 2;

  mesh.allTriIndices[3] = 0;
  mesh.allTriIndices[4] = 2;
  mesh.allTriIndices[5] = 3;

  // assign material per triangle
  //
  mesh.allmatIndices[0] = 0;
  mesh.allmatIndices[1] = 0;

  
  AppendShapeCube(&mesh);


  // now write this to .vsgf
  //
  HydraGeomData data;

  const size_t totalVertNumber     = mesh.allVerticesPos.size() / 4;
  const size_t totalMeshTriIndices = mesh.allTriIndices.size();

  data.setData(totalVertNumber,     &mesh.allVerticesPos[0], &mesh.allVerticesNorm[0], nullptr, &mesh.allVerticesTexCoord[0],
               totalMeshTriIndices, &mesh.allTriIndices[0], &mesh.allmatIndices[0]);
  
  data.setMaterialNameIndex("plane_mtl", 0); // vsgf store material names as list.
  data.setMaterialNameIndex("cube_mlt",  1); // it is important to put all names and indices;
                                             // if you have 27 different materials id in mesh.allmatIndices, you have to set all 27 here!!!

  std::ofstream fout(a_fileName.c_str(), std::ios::binary);

  data.write(fout);


}


int main(int argc, const char** argv)
{
  //_wmkdir(L"export"); // create directory where we put our scene. _wmkdir is not portable. Just an example.

  create_profile(L"export\\hydra_profile_generated.xml"); // Please do not change file name! It must be "hydra_profile_generated.xml"  (perhaps you can change it but we never did :) )

  create_settings(L"export\\settings.xml");               // Please do not change file name! It must be "settings.xml" (perhaps you can change it but we never did :) )

  create_geometry(L"export\\scene.vsgf");

  return 0;
}

