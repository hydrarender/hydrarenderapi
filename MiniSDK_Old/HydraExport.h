#pragma once

#include <vector>
#include <string>
#include <map>

#include <cstdint>


#ifdef __GNUC__

#include <map>
typedef std::map<std::string, int>         HashMapI;

#else

#include <hash_map>  
typedef stdext::hash_map<std::string, int>         HashMapI;

#endif

struct HydraGeomData
{
  HydraGeomData();
  ~HydraGeomData();

  //
  //
  void write(const std::string& a_fileName);
  void write(std::ostream& a_out);

  void read(const std::string& a_fileName);
  void read(std::istream& a_input);

  const std::string& getMaterialNameByIndex(uint32_t a_index)  const;
  uint32_t getMaterialIndexByName(const std::string& a_name);

  const std::vector<std::string>& _getMaterialsNamesVector() const; // not recomended to use this, but possible

  // common vertex attributes
  //
  uint32_t getVerticesNumber() const;
  const float* getVertexPositionsFloat4Array() const; 
  const float* getVertexNormalsFloat4Array()  const; 
  const float* getVertexTangentsFloat4Array()  const; 
  const float* getVertexTexcoordFloat2Array()  const; 

  // advanced attributes, for various types of lightmaps
  //
  const float* getVertexLightmapTexcoordFloat2Array()  const; 
  const float* getVertexSphericalHarmonicCoeffs()  const; 

  // per triangle data
  //
  uint32_t getIndicesNumber() const;                       // return 3*num_triangles
  const uint32_t* getTriangleVertexIndicesArray() const;   // 3*num_triangles
  const uint32_t* getTriangleMaterialIndicesArray() const; // 1*num_triangles 

  //
  //
  void setData(uint32_t a_vertNum, float* a_pos, float* a_norm, float* a_tangent, float* a_texCoord, 
               uint32_t a_indicesNum, uint32_t* a_triVertIndices, uint32_t* a_triMatIndices);

  void setMaterialNameIndex(const std::string a_name, uint32_t a_index);

protected:

  enum GEOM_FLAGS{ HAS_TANGENT = 1, 
                   HAS_LIGHTMAP_TEXCOORDS = 2, 
                   HAS_HARMONIC_COEFFS = 4 };

  // size info
  //
  uint64_t fileSizeInBytes;
  uint32_t verticesNum;
  uint32_t indicesNum;
  uint32_t materialsNum;
  uint32_t flags;


  //
  //
  float* m_positions;
  float* m_normals;
  float* m_tangents; 

  float* m_texcoords;

  uint32_t* m_triVertIndices;
  uint32_t* m_triMaterialIndices;

  char*     m_materialNames;
  uint32_t  m_matNamesTotalStringSize;
  
  char*   m_data; // this is a full dump of the file

  //
  //
  void freeMemIfNeeded();
  bool m_ownMemory;
  
  HashMapI                      m_matIndexByName;
  std::vector<std::string>      m_matNameByIndex;

};


